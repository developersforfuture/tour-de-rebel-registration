# Tour de Planet - Registration Form and Backend API


## Usage

Start the backend app via docker-compose:

```
docker-compose -f docker-compose_its.yml up -d
```

## Locations

Backend api: [backend/](backend/)
Frontend (React): [frontend/](frontend/)


## Build - Java

```bash
./bin/build_backend.sh
```

## Build DockerImager

```
./bin/buildAll.sh
```

## Run Frontend App:


```
cd frontend/
npm run start|build
```