const jwtSecret = require('../../common/config/env.config.js').jwt_secret,
    jwt = require('jsonwebtoken');
const crypto = require('crypto');

exports.login = (req, res) => {
    if (!req.body.userId) {
         return res.status(404).send({errors: 'No user found'});
    }
    const userId = req.body.userId
    try {
        let refreshId = userId + jwtSecret;
        let salt = crypto.randomBytes(16).toString('base64');
        let hash = crypto.createHmac('sha512', salt).update(refreshId).digest("base64");
        req.body.refreshKey = salt;
        let token = jwt.sign(req.body, jwtSecret);
        let b = new Buffer(hash);
        let refresh_token = b.toString('base64');
        return res.status(201).send({
            accessToken: token,
            refreshToken: refresh_token,
            userId: req.body.userId,
            mail: req.body.mail,
            permissionLevel: req.body.permissionLevel,
            provider: req.body.provider,
            username: req.body.username,
            name: req.body.firstName + ' ' + req.body.lastName,
        });
    } catch (err) {
        return res.status(500).send({errors: err});
    }
};

exports.refresh_token = (req, res) => {
    try {
        req.body = req.jwt;
        let token = jwt.sign(req.body, jwtSecret);
        res.status(201).send({id: token});
    } catch (err) {
        res.status(500).send({errors: err});
    }
};
