const UserModel = require('../../users/models/users.model');
const crypto = require('crypto');
const helper = require('../services/helper.service');


module.exports = {
    hasAuthValidFields,
    hasRegistrationValidFields,
    hasNoUsedUserNameAndEmail,
    isPasswordAndUserMatch
};

async function hasAuthValidFields (req, res, next) {
    let errors = [];

    if (req.body) {
        const hasUsername = !!(req.body.username);
        if (!hasUsername) {
            errors.push('Missing mail or username field');
        }
        if (!req.body.password) {
            errors.push('Missing password field');
        }

        if (errors.length) {
            return res.status(400).send({ errors: errors.join(',') });
        } else {
            return next();
        }
    } else {
        return res.status(400).send({ errors: 'Missing mail and password fields' });
    }
};

async function hasRegistrationValidFields(req, res, next) {
    let errors = [];

    if (req.body) {
        if (!req.body.mail) {
            errors.push('Missing mail field');
        }
        if (!req.body.password) {
            errors.push('Missing password field');
        }
        if (!req.body.passwordConfirm) {
            errors.push('Missing passwordConfirm field');
        }
        if (req.body.password != req.body.passwordConfirm) {
            errors.push('Password and confirmation do not match');
        }

        if (!/[a-z]/.test(req.body.password)) {
            errors.push('Password should contain lower case');
        }

        if (!/[A-Z]/.test(req.body.password)) {
            errors.push('Password should contain upper case');
        }

        if (!/\d/.test(req.body.password)) {
            errors.push('Password should contain numbers');
        }

        if (!/[@$!%*#?&]/.test(req.body.password)) {
            errors.push('Password should at leas one special character');
        }

        if (req.body.password.length < 8) {
            errors.push('Password should have at least 8 characters.');
        }

        if (errors.length) {
            return res.status(400).send({ errors: errors.join(',') });
        } else {
            return next();
        }
    } else {
        return res.status(400).send({ errors: 'Missing mail and password fields' });
    }
};

async function hasNoUsedUserNameAndEmail(req, res, next) {
    const userByMail = await UserModel .findBymail(req.body.mail);

    if (0 !== userByMail.length) {
        return res.status(400).send('Mail addresss still exists');
    }
    const userByUsername = await UserModel.findByUsername(req.body.username);

    if (0 !== userByUsername.length) {
        return res.status(400).send('Username still exists');
    }

    return next();
}

async function isPasswordAndUserMatch(req, res, next) {
    const userByMail = await UserModel.findBymail(req.body.username);
    const userByUsername = await UserModel.findByUsername(req.body.username);

    let user = null;
    if (0 !== userByMail.length) {
        user = userByMail;
    } else if (0 !== userByUsername.length) {
        user = userByUsername;
    }

    if (null === user) {
        return res.status(404).send({ errors: ['No user found'] });
    }

    let passwordFields = user[0].password.split('$');
    let salt = passwordFields[0];
    let passwordHash = passwordFields[1];
    let passwordData = helper.sha512(req.body.password, salt);
    if (passwordData.passwordHash === passwordHash) {
        req.body.userId = user[0]._id;
        return next();
    } else {
        return res.status(400).send({ errors: ['Invalid e-mail or password'] });
    }
};
