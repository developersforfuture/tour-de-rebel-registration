docker run -it --rm --name my-maven-project-tdr \
  -v $PWD/backend:/usr/src/mymaven \
  -w /usr/src/mymaven \
  -e PG_HOST=10.5.11.2 \
  -e PG_PORT=5432 \
  -e PG_DATABASE=tdr_test \
  -e PG_USERNAME=tdr_user \
  -e PG_PASSWORD=tdr_pw \
  maven:3.6-jdk-11 \
  mvn test
  