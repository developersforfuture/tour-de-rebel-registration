#!/usr/bin/env sh

set -ex

VERSION_TAG=${VERSION_TAG-'0.0.2'}
DOCKER_IMAGE=${DOCKER_IMAGE-'local'}
if [ "${DOCKER_IMAGE}" != 'local' ]
then
    # try to pull previously built images to utilize docker cache during new build
    docker pull $DOCKER_IMAGE/production:latest || true
fi;


# build and tag intermediate images and application image
docker build ./backend/ --target production -t $DOCKER_IMAGE/production:$VERSION_TAG
docker build ./frontend/ --target frontend-development -t $DOCKER_IMAGE/frontend-development:$VERSION_TAG

if [ "${DOCKER_IMAGE}" != 'local' ]
then
    docker push $DOCKER_IMAGE/production:$VERSION_TAG
    docker push $DOCKER_IMAGE/frontend-development:$VERSION_TAG
fi;
