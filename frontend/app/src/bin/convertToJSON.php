<?php

$file = fopen(__DIR__.'/raw_data.csv', 'r');

if (!$file) {
    echo "File not readable";
    return;
}

function getPoint($pointData, $id)
{
    if (!isset($pointData[5])) {
        echo "no data in set".PHP_EOL;
        var_dump($pointData);
        return null;
    }
    if (!preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $pointData[5])) {
        echo "no valid geo coordinate: '".$pointData[5]."'\n";

        return null;
    }

    list($lat, $lng) = explode(',', $pointData[5]);
    $point = new \stdClass();
    $point->latitude = (float)$lat;
    $point->longitude = (float)$lng;
    $point->name = $pointData[3];
    $point->date = $pointData[1];
    $point->id = $id;

    return $point;
}


$segments = [];
$rawData = [];

while (($data = fgetcsv($file, 1000, ",")) !== false) {
    $rawData[] = $data;
}

foreach ($rawData as $counter => $rawDataPoint) {
    if ($counter === 0 || 1 === $counter) {
        continue;
    }

    $num = count($rawDataPoint);
    if ($num < 13) {
        echo "not enough cols found: ".PHP_EOL;
        var_dump($rawDataPoint);
        continue;
    }
    $start = getPoint($rawDataPoint, "100".$counter);
    if (!isset($rawData[$counter + 1])) {
        echo "No next data set not available\n";

        continue;
    }
    $end = getPoint($rawData[$counter + 1], "100".$counter + 1);
    if (!$start || !$end) {
        continue;
    }

    $segment = new \stdClass();
    $segment->id = $counter;
    $segment->name = $rawDataPoint[3].' - '.$rawDataPoint[4];
    $segment->start = $start;
    $segment->end = $end;
    $segment->distance = $rawDataPoint[6];
    $segment->climb = $rawDataPoint[7];
    $segment->decent = $rawDataPoint[8];
    $segment->organizer = $rawDataPoint[11];
    $segment->description = $rawDataPoint[12];

    $segments[] = $segment;
}

$db = json_encode($segments, JSON_PRETTY_PRINT);
file_put_contents(__DIR__.'/segments.json', $db);
