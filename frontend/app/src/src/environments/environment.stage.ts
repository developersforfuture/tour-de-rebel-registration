export const environment = {
  production: true,
  apiUrl: 'http://localhost:3032',
  backendUrl: 'https://staging.tourdeplanet.org',
  locationApiUrl: 'https://nominatim.openstreetmap.org/'
};
