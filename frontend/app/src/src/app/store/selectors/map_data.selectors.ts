import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromMap from '../reducers/map_data.reducer';

export const getMapState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.map
);

export const getMapDonationEntities = createSelector(
    getMapState,
    fromMap.getMapDonationEntities
);

export const getAllDonationsOnMap = createSelector(getMapDonationEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});


export const getMapTourEntities = createSelector(
    getMapState,
    fromMap.getMapTourEntities
);

export const getAllToursOnMap = createSelector(getMapTourEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});


export const getMapEventEntities = createSelector(
    getMapState,
    fromMap.getMapEventEntities
);

export const getAllEventsOnMap = createSelector(getMapEventEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getMapDataLoaded = createSelector(
    getMapState,
    fromMap.getLoaded
);

export const getMapDataLoading = createSelector(
    getMapState,
    fromMap.getLoading
);
