import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromAttendee from '../reducers/attendee.reducer';

export const getAttendeeDonationState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.donations
);

export const getAttendeeDonationEntities = createSelector(
    getAttendeeDonationState,
    fromAttendee.getDonationEntities
);


export const getAllAttendeeDonations = createSelector(getAttendeeDonationEntities, entities => {
    if (!entities) {
        return [];
    }
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getAttendeeDonationsLoaded = createSelector(
    getAttendeeDonationState,
    fromAttendee.getLoaded
);
export const getAttendeeDonationsLoading = createSelector(
    getAttendeeDonationState,
    fromAttendee.getLoading
);
