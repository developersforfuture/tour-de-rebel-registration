import {createSelector} from '@ngrx/store';
import * as fromFeature from '../reducers';

export const getView = createSelector(
    fromFeature.getAppState,
    state => state.view
);

export const getSiteNavigationVisibility = createSelector(
    getView,
    state => state.sidnavVisibility
);

export const getSelectedElements = createSelector(
    getView,
    state => state.selected
);

export const getSelectedTours = createSelector(
    getSelectedElements,
    elements => elements.tours
);


export const getSelectedEvents = createSelector(
    getSelectedElements,
    elements => elements.events
);

export const getSelectedSegments = createSelector(
    getSelectedElements,
    elements => elements.segments
);
