import {Action} from '@ngrx/store';
import { MapModel } from 'src/app/modules/map/models/map.model';
import { TourAttendeeDonation } from 'src/app/modules/donations/tour_attendee_donation.model';

export const LOAD_MAP = '[Map] Load map data';
export const LOAD_MAP_FAIL = '[Map] Load map data failed';
export const LOAD_MAP_SUCCESS = '[Map] Load map data succeeded';

export const ADD_DONATION = '[Map] Add a new donation to map data.';

export class LoadMapAction implements Action {
    readonly type = LOAD_MAP;
}

export class LoadMapFailAction implements Action {
    readonly type = LOAD_MAP_FAIL;
    constructor(public payload: any[]) {}
}

export class LoadMapSuccessAction implements Action {
    readonly type = LOAD_MAP_SUCCESS;
    constructor(public payload: MapModel) {}
}

export class AddDonationAction implements Action {
    readonly type = ADD_DONATION;
    constructor(public payload: TourAttendeeDonation) {}
}


// action types
export type MapAction =
    | LoadMapAction
    | LoadMapFailAction
    | LoadMapSuccessAction
    | AddDonationAction;
