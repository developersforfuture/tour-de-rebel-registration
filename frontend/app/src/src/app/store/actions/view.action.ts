import {Action} from '@ngrx/store';
import { TourModel } from 'src/app/modules/tour/models/tour.model';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';

export const OPEN_SIDENAV = '[View] open siedenav area';
export const CLOSE_SIDENAV = '[View] close siedenav area';

export const TOUR_SECTED = '[View] tour selected';
export const SEGMENT_SECTED = '[View] segment selected';
export const EVENT_SECTED = '[View] event selected';
export const ALL_DESELECT = '[View] remove all selections on map';

export class OpenSidenavAction implements Action {
    readonly type = OPEN_SIDENAV;
    constructor() {}
}

export class CloseSidenavAction implements Action {
    readonly type = CLOSE_SIDENAV;
    constructor() {}
}

export class TourSelectedAction implements Action {
    readonly type = TOUR_SECTED;
    constructor(public payload: TourModel) {}
}

export class SegmentSelectedAction implements Action {
    readonly type = SEGMENT_SECTED;
    constructor(public payload: TourSegmentModel) {}
}

export class EventSelectedAction implements Action {
    readonly type = EVENT_SECTED;
    constructor(public payload: TourEvent) {}
}

export class AllDeselectAction implements Action {
    readonly type = ALL_DESELECT;
    constructor() {}
}

export type ViewActionSet = OpenSidenavAction
    | CloseSidenavAction
    | TourSelectedAction
    | EventSelectedAction
    | SegmentSelectedAction
    | AllDeselectAction;
