import {Action} from '@ngrx/store';
import { EditAttendeeProfileModel } from 'src/app/modules/account/models/edit_attendee.model';
import { TourAttendeeDonation } from 'src/app/modules/donations/tour_attendee_donation.model';
import { EditAttendeeTourProfileModel } from 'src/app/modules/account/models/edit_attendee_profile.model';
import { EditAttendeePasswordModel } from 'src/app/modules/account/models/edit_attendee_password.model';

export const LOAD_ATTENDEE_DONATION = '[Attendee] Load Attendee donation.';
export const LOAD_ATTENDEE_DONATION_FAILED = '[Attendee] Load Attendee donation failed.';
export const LOAD_ATTENDEE_DONATION_SUCCESS = '[Attendee] Load Attendee donation success.';

export const UPDATE_ATTENDEE_PROFILE = '[Attendee] Update attendee profile.';
export const UPDATE_ATTENDEE_PROFILE_FAIL = '[Attendee] Update attendee profile failed.';
export const UPDATE_ATTENDEE_PROFILE_SUCCESS = '[Attendee] Update attendee profile succeeded.';

export const ADD_ATTENDEE_DONATION = '[Attendee] Add new donation.';
export const ADD_ATTENDEE_DONATION_FAIL = '[Attendee] Add new donation failed.';
export const ADD_ATTENDEE_DONATION_SUCCESS = '[Attendee] Add new donation succeeded.';

export const UPDATE_ATTENDEE_TOUR_PROFILE = '[Attendee] Update tour profile.';
export const UPDATE_ATTENDEE_TOUR_PROFILE_FAIL = '[Attendee] Update tour profile failed.';
export const UPDATE_ATTENDEE_TOUR_PROFILE_SUCCESS = '[Attendee] Update tour profile succeeded.';

export const UPDATE_ATTENDEE_PASSWORD = '[Attendee] Update attendee password.';
export const UPDATE_ATTENDEE_PASSWORD_FAIL = '[Attendee] Update attendee password failed.';
export const UPDATE_ATTENDEE_PASSWORD_SUCCESS = '[Attendee] Update attendee password succeded.';


export class LoadAttendeeDonationAction implements Action {
    readonly type = LOAD_ATTENDEE_DONATION;
    constructor() {}
}

export class LoadAttendeeDonationFailAction implements Action {
    readonly type = LOAD_ATTENDEE_DONATION_FAILED;
    constructor(public payload: any[]) {}
}

export class LoadAttendeeDonationSuccesscAction implements Action {
    readonly type = LOAD_ATTENDEE_DONATION_SUCCESS;
    constructor(public payload: TourAttendeeDonation[]) {}
}

export class UpdateAttendeeProfile implements Action {
    readonly type = UPDATE_ATTENDEE_PROFILE;
    constructor(public payload: EditAttendeeProfileModel) {}
}

export class UpdateAttendeeProfileFail implements Action {
    readonly type = UPDATE_ATTENDEE_PROFILE_FAIL;
    constructor(public payload: any[]) {}
}

export class UpdateAttendeeProfileSuccess implements Action {
    readonly type = UPDATE_ATTENDEE_PROFILE_SUCCESS;
    constructor(public payload: EditAttendeeProfileModel) {}
}

export class AddAttendeeDonation implements Action {
    readonly type = ADD_ATTENDEE_DONATION;
    constructor(public payload: TourAttendeeDonation) {}
}

export class AddAttendeeDonationFail implements Action {
    readonly type = ADD_ATTENDEE_DONATION_FAIL;
    constructor(public payload: any[]) {}
}

export class AddAttendeeDonationSuccess implements Action {
    readonly type = ADD_ATTENDEE_DONATION_SUCCESS;
    constructor(public payload: TourAttendeeDonation) {}
}

export class UpdateAttendeeTourProfileAction implements Action {
    readonly type = UPDATE_ATTENDEE_TOUR_PROFILE;
    constructor(public payload: EditAttendeeTourProfileModel) {}
}

export class UpdateAttendeeTourProfileFailAction implements Action {
    readonly type = UPDATE_ATTENDEE_TOUR_PROFILE_FAIL;
    constructor(public payload: any[]) {}
}

export class UpdateAttendeeTourProfileSuccessAction implements Action {
    readonly type = UPDATE_ATTENDEE_TOUR_PROFILE_SUCCESS;
    constructor(public payload: EditAttendeeTourProfileModel) {}
}

export class UpdateAttendeePasswordAction implements Action {
    readonly type = UPDATE_ATTENDEE_PASSWORD;
    constructor(public payload: EditAttendeePasswordModel) {}
}

export class UpdateAttendeePasswordFailAction implements Action {
    readonly type = UPDATE_ATTENDEE_PASSWORD_FAIL;
    constructor(public payload: any[]) {}
}

export class UpdateAttendeePasswordSuccessAction implements Action {
    readonly type = UPDATE_ATTENDEE_PASSWORD_SUCCESS;
    constructor(public payload: EditAttendeePasswordModel) {}
}

// action types
export type AttendeeAction =
    | LoadAttendeeDonationAction
    | LoadAttendeeDonationFailAction
    | LoadAttendeeDonationSuccesscAction
    | UpdateAttendeeProfile
    | UpdateAttendeeProfileFail
    | UpdateAttendeeProfileSuccess
    | AddAttendeeDonation
    | AddAttendeeDonationFail
    | AddAttendeeDonationSuccess
    | UpdateAttendeeTourProfileAction
    | UpdateAttendeeTourProfileFailAction
    | UpdateAttendeeTourProfileSuccessAction;
