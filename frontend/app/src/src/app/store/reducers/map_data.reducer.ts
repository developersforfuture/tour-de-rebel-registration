import * as fromMap from '../actions/map_data.action';
import { TourModel as Tour } from 'src/app/modules/tour/models/tour.model';
import { TourAttendeeDonation } from 'src/app/modules/donations/tour_attendee_donation.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';

export interface MapDataState {
    donations: {[id: number]: TourAttendeeDonation};
    tours: {[id: number]: Tour};
    events: {[id: number]: TourEvent};
    loaded: boolean;
    loading: boolean;
}

export const initialState: MapDataState = {
    donations: {},
    tours: {},
    events: {},
    loaded: false,
    loading: false,
};

export function reducer(
    state = initialState,
    action: fromMap.MapAction
): MapDataState {
    switch (action.type) {
        case fromMap.LOAD_MAP: {
            return {
                ...state,
                loading: true,
            };
        }

        case fromMap.LOAD_MAP_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false,
            };
        }

        case fromMap.LOAD_MAP_SUCCESS: {
            const mapData = action.payload;

            const tours = mapData.tours.reduce(
                (e: { [id: number]: Tour }, tour: Tour) => {
                    return { ...e, [tour.id]: tour};
                },
                { ...state.tours});

            const events = mapData.events.reduce(
                (e: { [id: number]: TourEvent }, event: TourEvent) => {
                    return { ...e, [event.id]: event};
                },
                { ...state.events});

            const donations = mapData.donations.reduce(
                (e: { [id: number]: TourAttendeeDonation }, donation: TourAttendeeDonation) => {
                    return { ...e, [donation.id]: donation};
                },
                { ...state.donations});

            return {
                ...state,
                loading: false,
                loaded: true,
                tours,
                events,
                donations
            };
        }

        case fromMap.ADD_DONATION: {
            const donation = action.payload;

            const donations = {...state.donations, [donation.id]: donation};

            return {
                ...state,
                loading: false,
                loaded: true,
                donations
            };
        }
    }

    return state;
}

export const getMapTourEntities = (state: MapDataState) => state.tours;
export const getMapEventEntities = (state: MapDataState) => state.events;
export const getMapDonationEntities = (state: MapDataState) => state.donations;
export const getLoading = (state: MapDataState) => state.loading;
export const getLoaded = (state: MapDataState) => state.loaded;
