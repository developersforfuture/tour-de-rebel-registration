import * as fromTours from '../actions/tours.action';
import { TourModel as Tour } from 'src/app/modules/tour/models/tour.model';

export interface ToursState {
    entities: {[id: number]: Tour};
    loaded: boolean;
    loading: boolean;
}

export const initialState: ToursState = {
    entities: {},
    loaded: false,
    loading: false,
};

export function reducer(
    state = initialState,
    action: fromTours.ToursAction
): ToursState {
    switch (action.type) {
        case fromTours.LOAD_TOURS:
        case fromTours.CREATE_NEW_TOUR:
        case fromTours.UPDATE_TOUR:
        case fromTours.REMOVE_TOUR: {
            return {
                ...state,
                loading: true,
            };
        }

        case fromTours.LOAD_TOURS_FAIL:
        case fromTours.REMOVE_TOUR_FAIL:
        case fromTours.UPDATE_TOUR_FAIL:
        case fromTours.CREATE_NEW_TOUR_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false,
            };
        }

        case fromTours.LOAD_TOURS_SUCCESS: {
            const tours = action.payload;

            const entities = tours.reduce(
                (e: { [id: number]: Tour }, tour: Tour) => {
                    return { ...e, [tour.id]: tour};
                },
                { ...state.entities}
            );

            return {
                ...state,
                loading: false,
                loaded: true,
                entities,
            };
        }

        case fromTours.CREATE_NEW_TOUR_SUCCESS: {
            const tour = action.payload;

            const entities = {...state.entities, [tour.id]: tour};

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }

        case fromTours.UPDATE_TOUR_SUCCESS: {
            const tour = action.payload;

            const entities = {...state.entities, [tour.id]: tour};

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }

        case fromTours.REMOVE_TOUR_SUCCESS: {
            const id = action.payload;
            const entities = { ... state.entities};
            delete entities[id];

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }
    }

    return state;
}

export const getTourEntities = (state: ToursState) => state.entities;
export const getLoading = (state: ToursState) => state.loading;
export const getLoaded = (state: ToursState) => state.loaded;
