import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromView from './view.reducer';
import * as fromTour from './tour.reducer';
import * as fromEvents from './tour_events.reducer';
import * as fromAttendee from './attendee.reducer';
import * as fromMap from './map_data.reducer';

export interface AppState {
    view: fromView.ViewState;
    tours: fromTour.ToursState;
    events: fromEvents.EventsState;
    donations: fromAttendee.AttendeeDonationState;
    map: fromMap.MapDataState;
}

export const reducers: ActionReducerMap<AppState> = {
    view: fromView.reducer,
    tours: fromTour.reducer,
    events: fromEvents.reducer,
    donations: fromAttendee.reducer,
    map: fromMap.reducer
};

export const getAppState = createFeatureSelector<AppState>('app');
