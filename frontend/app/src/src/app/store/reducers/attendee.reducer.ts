import * as fromAttendee from '../actions/attendee.action';
import { TourAttendeeDonation } from 'src/app/modules/donations/tour_attendee_donation.model';

export interface AttendeeDonationState {
    entities: {[id: string]: TourAttendeeDonation};
    loaded: boolean;
    loading: boolean;
}

export const initialState: AttendeeDonationState = {
    entities: {},
    loaded: false,
    loading: false,
};

export function reducer(
    state = initialState,
    action: fromAttendee.AttendeeAction
): AttendeeDonationState {
    switch (action.type) {
        case fromAttendee.LOAD_ATTENDEE_DONATION:
        case fromAttendee.ADD_ATTENDEE_DONATION: {
            return {
                ...state,
                loading: true,
            };
        }

        case fromAttendee.ADD_ATTENDEE_DONATION_FAIL:
        case fromAttendee.LOAD_ATTENDEE_DONATION_FAILED: {
            return {
                ...state,
                loading: false,
                loaded: false,
            };
        }

        case fromAttendee.LOAD_ATTENDEE_DONATION_SUCCESS: {
            const events = action.payload;

            const entities = events.reduce(
                (e: { [id: number]: TourAttendeeDonation }, event: TourAttendeeDonation) => {
                    return { ...e, [event.id]: event};
                },
                { ...state.entities}
            );

            return {
                ...state,
                loading: false,
                loaded: true,
                entities,
            };
        }

        case fromAttendee.ADD_ATTENDEE_DONATION_SUCCESS: {
            const entity = action.payload;
            const entities = { ...state.entities, [entity.id]: entity};

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }
    }

    return state;
}

export const getDonationEntities = (state: AttendeeDonationState) => state.entities;
export const getLoading = (state: AttendeeDonationState) => state.loading;
export const getLoaded = (state: AttendeeDonationState) => state.loaded;
