
import * as actions from '../actions';
import { TourModel } from 'src/app/modules/tour/models/tour.model';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';

export interface ViewState {
    sidnavVisibility: boolean;
    selected: {
        tours: TourModel[],
        segments: TourSegmentModel[],
        events: TourEvent[]
    };
}

export const initialState: ViewState = {
    sidnavVisibility: true,
    selected: {
        segments: [],
        tours: [],
        events: []
    }
};

export function reducer(
    state = initialState,
    action: actions.ViewActionSet
): ViewState {
    switch (action.type) {
        case actions.OPEN_SIDENAV:
            return {...state, sidnavVisibility: true };

        case actions.CLOSE_SIDENAV:
            return {...state, sidnavVisibility: false };

        case actions.SEGMENT_SECTED:
            const segments = state.selected.segments;
            segments.push( action.payload as TourSegmentModel);
            return {...state, selected: {...state.selected, segments}};

        case actions.EVENT_SECTED:
            const events = [ ...state.selected.events, action.payload];
            return {...state, selected: {...state.selected, events}};

        case actions.TOUR_SECTED:
            const currentTours = state.selected.tours;
            currentTours.push(action.payload as TourModel);
            return {...state, selected: {...state.selected, tours: currentTours}};

        case actions.ALL_DESELECT:
            return {...state, selected: {...state.selected, tours: [], events: [], segments: []}};
    }

    return state;
}
