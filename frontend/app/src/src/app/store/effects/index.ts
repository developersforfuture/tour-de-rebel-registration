import { ToursEffect } from './tour.effect';
import { AttendeeEffect } from './attendee.effect';
import { MapDataEffect } from './map_data.effect';

export const effects = [ToursEffect, AttendeeEffect, MapDataEffect];

export * from './tour.effect';
export * from './attendee.effect';
export * from './map_data.effect';
