import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { LogoutComponent } from './components/logout/logout.component';
import { AuthenticationService } from './services/authentication.service';
import { AttendeesService } from './services/attendees.service';
import { AccountComponent } from './components/account/account.component';
import { HttpClientModule } from '@angular/common/http';
import {
  MatCardModule,
  MatDatepickerModule,
  MatInputModule,
  MatButtonModule,
  MatListModule,
  MatSelectModule,
  MatCheckboxModule } from '@angular/material';
import { EditAttendeeComponent } from './components/edit-attendee-profile/edit_attendee.component';
import { EditAttendeeProfileComponent } from './components/edit-attendee-tour-profile/edit_attendee_profile.component';
import { EditAttendeePasswordComponent } from './components/edit-password/edit_attendee_password.component';
import { RegistrationFormComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';

export const accountRoutes: Routes = [
  { path: 'dashboard', component: EditAttendeeComponent },
  { path: 'logout', component: LogoutComponent},
  {
    path: 'profile',
    component: EditAttendeeProfileComponent
  },
  { path: 'password',
    component: EditAttendeePasswordComponent
  }
];

@NgModule({
  declarations: [
    AccountComponent,
    EditAttendeeComponent,
    EditAttendeePasswordComponent,
    EditAttendeeProfileComponent,
    RegistrationFormComponent,
    LoginComponent,
    LogoutComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(accountRoutes),
    ReactiveFormsModule,
    HttpClientModule,
    MatCardModule,
    MatDatepickerModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
  ],
  providers: [AttendeesService, AuthenticationService],
  exports: [AccountComponent]
})
export class AccountModule { }
