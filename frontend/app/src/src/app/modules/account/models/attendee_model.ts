import { TourAttendeeDonation } from 'src/app/modules/donations/tour_attendee_donation.model';
import { EditAttendeeTourProfileModel } from './edit_attendee_profile.model';
import { EditAttendeeProfileModel } from './edit_attendee.model';

export interface Attendee extends EditAttendeeProfileModel, EditAttendeeTourProfileModel {
    id: number;
    donations: TourAttendeeDonation[];
}
