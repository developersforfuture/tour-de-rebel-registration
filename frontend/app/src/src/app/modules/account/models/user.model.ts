
export class User {
    id: number;
    username: string;
    mail?: string;
}

export class UserToken {
    token: string;
}
