import { Point, PointForm } from './../../tour/models/point.model';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MeanOfTransportationModel } from './../../../shared/models/mean_of_transportaion.model';

export interface EditAttendeeTourProfileModel {
    id: number;
    isBuildOwnTour: boolean;
    isJoinOtherTour: boolean;
    isPress: boolean;
    isOrga: boolean;
    home: Point;
    daysToParticipate: number;
    distanceToParticipate: number;
    meanOfTransportation: number|MeanOfTransportationModel;
    unit: string;
}


export class EditAttendeeTourProfileForm {
    id = new FormControl();
    isBuildOwnTour = new FormControl();
    isJoinOtherTour = new FormControl();
    isPress = new FormControl();
    isOrga = new FormControl();
    home = new FormControl();
    daysToParticipate = new FormControl();
    distanceToParticipate = new FormControl();
    meanOfTransportation = new FormControl();
    unit = new FormControl();
    constructor(model: EditAttendeeTourProfileModel) {
        if (model.id) {
            this.id.patchValue(model.id);
        }

        this.isBuildOwnTour.patchValue(model.isBuildOwnTour);
        this.isOrga.patchValue(model.isOrga);
        this.isPress.patchValue(model.isPress);
        this.isBuildOwnTour.patchValue(model.isJoinOtherTour);
        this.isBuildOwnTour.patchValue(model.isBuildOwnTour);
        this.daysToParticipate.patchValue(model.daysToParticipate);
        this.distanceToParticipate.patchValue(model.distanceToParticipate);
        this.unit.patchValue(model.unit);
        this.home.patchValue(model.home);

        if (model.meanOfTransportation && model.meanOfTransportation.hasOwnProperty('id')) {
            const transportation = model.meanOfTransportation as MeanOfTransportationModel;
            this.meanOfTransportation.patchValue(transportation.id);
        }
    }
}
