export interface EditAttendeeProfileModel {
    id: number;
    username: string;
    mail: string;
    firstName: string;
    secondName: string;
    organisation: number;
    country: number;
}
