import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { environment } from '../../../../environments/environment';
import { EditAttendeeTourProfileModel } from '../models/edit_attendee_profile.model';
import { EditAttendeePasswordModel } from '../models/edit_attendee_password.model';
import { EditAttendeeProfileModel } from '../models/edit_attendee.model';
import { TourAttendeeRegistrationModel } from '../models/tour_attendee_registration.model';
import { ConfigService } from 'src/app/shared/services';
import { TourAttendeeDonation } from '../../donations/tour_attendee_donation.model';

@Injectable()
export class AttendeesService {
    constructor(private http: HttpClient, private configService: ConfigService) {}

    create(registration: TourAttendeeRegistrationModel): Observable<{} | string> {
        return this.http
            .post<TourAttendeeRegistrationModel>(`${environment.backendUrl}/tour-management/register`, {
                username: registration.username,
                email: registration.mail,
                plainPassword: registration.password,
                newsletterAccepted: false,
                termsAccepted: true
            })
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }

    getCurrentTourAttendeeProfile(): Observable<EditAttendeeTourProfileModel> {
        return this.http
            .get<EditAttendeeTourProfileModel>(`${environment.backendUrl}/tour-management/tourattendee/tourprofile `)
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }

    updateTourProfile(attendee: EditAttendeeTourProfileModel): Observable<EditAttendeeTourProfileModel> {
        return this.http
            .put<EditAttendeeTourProfileModel>(`${environment.backendUrl}/tour-management/tourattendee/tourprofile`, attendee)
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }

    getCurrentAttendeeProfile(): Observable<EditAttendeeProfileModel> {
        return this.http
            .get<EditAttendeeProfileModel>(`${environment.backendUrl}/tour-management/tourattendee/profile`)
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }

    updateTourAttendeePassword(attendee: EditAttendeePasswordModel): Observable<EditAttendeePasswordModel> {
        return this.http
            .put<EditAttendeePasswordModel>(`${environment.backendUrl}/tour-management/tourattendee/password`, attendee)
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }

    updateTourAttendeeProfile(attendee: EditAttendeeProfileModel): Observable<EditAttendeeProfileModel> {
        return this.http
            .put<EditAttendeeProfileModel>(`${environment.backendUrl}/tour-management/tourattendee/profile`, attendee)
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }
}
