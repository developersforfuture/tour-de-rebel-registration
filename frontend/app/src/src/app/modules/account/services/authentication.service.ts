import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { User, UserToken } from 'src/app/modules/account/models/user.model';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserTokenSubject: BehaviorSubject<UserToken>;
    public currentUserToken: Observable<UserToken>;

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;


    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        this.currentUserTokenSubject = new BehaviorSubject<UserToken>(JSON.parse(localStorage.getItem('currentUserToken')));
        this.currentUserToken = this.currentUserTokenSubject.asObservable();
    }

    public get currentUserTokenValue(): UserToken {
        return this.currentUserTokenSubject.value;
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string): Observable<UserToken> {
        return this.http.post<UserToken>(environment.backendUrl + '/tour-management/login_check', { username, password })
            .pipe(
                map((token: UserToken) => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUserToken', JSON.stringify(token));
                this.currentUserTokenSubject.next(token);
                this.http.get<User>(environment.backendUrl + '/tour-management/user').subscribe(user => {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                });
                return token;
            }),
            catchError((error: any) => {
                return Observable.throw(error);
            })
            );
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUserToken');
        this.currentUserTokenSubject.next(null);
        this.currentUserSubject.next(null);
    }
}
