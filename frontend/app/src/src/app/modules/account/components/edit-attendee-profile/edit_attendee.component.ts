import { Component, OnDestroy, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Organisation } from 'src/app/shared/models/organisation.model';
import { Country } from 'src/app/shared/models/country.model';
import { EditAttendeeProfileModel } from '../../models/edit_attendee.model';
import { AttendeesService } from '../../services/attendees.service';
import { DataService, ModalService } from 'src/app/shared/services';
import { PlaceModel } from './../../../map/models/place.model';
import { CustomValidators } from 'src/app/modules/account/form/validator/custom-validators';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';

@Component({
    selector: 'app-edit-attendee',
    templateUrl: './edit_attendee.component.html',
    styleUrls: ['./edit_attendee.component.scss']
})
export class EditAttendeeComponent implements AfterViewInit, OnDestroy {
    editForm: FormGroup;
    organisations$: Observable<Organisation[]>;
    countries$: Observable<Country[]>;
    attendee: EditAttendeeProfileModel;
    $autoCompleteItems: Observable<PlaceModel[]>;
    constructor(
        private fb: FormBuilder,
        private attendeesService: AttendeesService,
        private restService: DataService,
        private router: Router,
        private notificationService: NotificationService
    ) {
        this.editForm = this.fb.group({
            username: ['', Validators.compose([Validators.required])],
            mail: [null, Validators.compose([
                Validators.email,
                Validators.required,
                CustomValidators.patternValidator(
                    /^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$/,
                    {hasMailPattern: true}
                )
            ])
             ],
            firstName: [''],
            secondName: [''],
            organisation: [0, Validators.required],
            country: [0, Validators.required],
        });
    }

    get invalid(): boolean {
        return this.usernameControlInvalid || this.mailControlInvalid;
    }

    get usernameControl(): FormControl {
        return this.editForm.get('username') as FormControl;
    }

    get mailControl(): FormControl {
        return this.editForm.get('mail') as FormControl;
    }

    get firstNameControl(): FormControl {
        return this.editForm.get('firstName') as FormControl;
    }

    get secondNameControl(): FormControl {
        return this.editForm.get('secondName') as FormControl;
    }

    get organisationControl(): FormControl {
        return this.editForm.get('organisation') as FormControl;
    }

    get countryControl(): FormControl {
        return this.editForm.get('country') as FormControl;
    }
    get usernameControlInvalid(): boolean {
        return  this.usernameControl.touched && this.usernameControl.invalid;
    }

    get mailControlInvalid(): boolean {
        return this.mailControl.hasError('required')
            && this.mailControl.touched
            && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.mailControl.value);
    }

    ngAfterViewInit() {
        this.attendeesService.getCurrentAttendeeProfile().subscribe(attendee => {
            this.editForm.patchValue(attendee);
            this.attendee = attendee;
        });
        this.organisations$ = this.restService.getOrganisations();
        this.countries$ = this.restService.getCountries();
        ModalService.on('div.modal.registration');
    }

    ngOnDestroy(): void {
        ModalService.off();
    }

    onClose(): void {
        ModalService.off();
    }

    onSubmit(): void {
        const attendee: EditAttendeeProfileModel = {
            id: this.attendee.id,
            username: this.usernameControl.value,
            mail: this.mailControl.value,
            firstName: this.firstNameControl.value,
            secondName: this.secondNameControl.value,
            organisation: this.organisationControl.value,
            country: this.countryControl.value,
        };

        this.attendeesService.updateTourAttendeeProfile(attendee).subscribe(() => {
            this.notificationService.success('Attendee tour profile updated', 'Success', true);
            this.router.navigate(['']);
        }, (err) => {
            let message = '';
            if (err.errors) {
                for (const key in err.errors) {
                    if (err.errors.hasOwnProperty(key)) {
                        message += err.errors[key];
                    }
                }
            }
            this.notificationService.error(message, err.title);
        });
    }
}
