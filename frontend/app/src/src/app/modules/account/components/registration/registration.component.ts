import { Component, OnDestroy, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CustomValidators } from 'src/app/modules/account/form/validator/custom-validators';
import { DataService, ModalService } from 'src/app/shared/services';
import { AttendeesService } from '../../services/attendees.service';
import { TourAttendeeRegistrationModel } from '../../models/tour_attendee_registration.model';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';

@Component({
    selector: 'app-registration-form',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationFormComponent implements AfterViewInit, OnDestroy {
    signInForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private attendeesService: AttendeesService,
        private router: Router,
        private notify: NotificationService
    ) {
        this.signInForm = this.fb.group({
            username: ['', Validators.compose([Validators.required])],
            password: [null, Validators.compose([
                Validators.required,
                CustomValidators.patternValidator(/\d/, { hasNumber: true }),
                CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
                CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
                CustomValidators.patternValidator(/[@$!%*#?&]/, {hasSpecialCharacters: true}),
                Validators.minLength(8),
            ])],
            passwordConfirm: [null, Validators.compose([Validators.required])],
            mail: [null, Validators.compose([
                Validators.email,
                Validators.required,
                CustomValidators.patternValidator(
                    /^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$/,
                    {hasMailPattern: true}
                )
            ])],
            newsletterAccepted: [false],
            termsAccepted: [false, Validators.requiredTrue],
        },
        {
            validator: CustomValidators.passwordMatchValidator
        });
    }

    get invalid(): boolean {
        return this.usernameControlInvalid || this.mailControlInvalid || this.passwordControlInvalid || this.termsAcceptedControlInvalid;
    }

    get usernameControl(): FormControl {
        return this.signInForm.get('username') as FormControl;
    }

    get mailControl(): FormControl {
        return this.signInForm.get('mail') as FormControl;
    }

    get passwordControl(): FormControl {
        return this.signInForm.get('password') as FormControl;
    }

    get passwordConfirmControl(): FormControl {
        return this.signInForm.get('passwordConfirm') as FormControl;
    }

    get newsletterAcceptedControl(): FormControl {
        return this.signInForm.get('newsletterAccepted') as FormControl;
    }

    get termsAcceptedControl(): FormControl {
        return this.signInForm.get('termsAccepted') as FormControl;
    }

    get usernameControlInvalid(): boolean {
        return  this.usernameControl.touched && this.usernameControl.invalid;
    }

    get mailControlInvalid(): boolean {
        return this.mailControl.hasError('required')
            && this.mailControl.touched
            && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.mailControl.value);
    }

    get passwordHasMinLength(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('minlength');
    }

    get passwordHasNumber(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasNumber');
    }

    get passwordHasCapitalCase(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasCapitalCase');
    }

    get passwordHasSmallCase(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasSmallCase');
    }

    get passwordHasSpecialCharacters(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasSpecialCharacters');
    }

    get passwordControlInvalid(): boolean {
        return this.passwordControl.touched && this.passwordControl.invalid;
    }

    get passwordConfirmControlInvalid(): boolean {
        return this.passwordConfirmControl.touched && this.passwordConfirmControl.invalid;
    }

    get termsAcceptedControlInvalid(): boolean {
        return this.signInForm.dirty && !this.termsAcceptedControl.valid;
    }


    ngAfterViewInit() {
        ModalService.on('div.modal.registration');
    }

    ngOnDestroy(): void {
        ModalService.off();
    }

    onClose(): void {
        ModalService.off();
    }

    onSubmit(): void {
        const registration: TourAttendeeRegistrationModel = {
            username: this.usernameControl.value,
            password: this.passwordControl.value,
            passwordConfirm: this.passwordConfirmControl.value,
            mail: this.mailControl.value,
            termsAccepted: this.termsAcceptedControl.value,
            newsletterAccepted: this.newsletterAcceptedControl.value
        };

        this.attendeesService.create(registration).subscribe(() => {
            this.notify.success('Successfully created your account, please have a look into your mails to verify it now.', 'Success', true);
            this.router.navigate(['']);
        }, (err) => {
            let message = '';
            if (err.errors) {
                for (const key in err.errors) {
                    if (err.errors.hasOwnProperty(key)) {
                        message += err.errors[key];
                    }
                }
            }
            this.notify.error(message, err.title);
        });
    }
}
