import { Component,  OnDestroy, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { EditAttendeeTourProfileModel, EditAttendeeTourProfileForm } from '../../models/edit_attendee_profile.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { PlaceModel } from './../../../map/models/place.model';
import { AttendeesService } from '../../services/attendees.service';
import { DataService, LocationApiService, ModalService } from 'src/app/shared/services';
import { HttpError } from 'src/app/shared/models/http_error.mdel';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';

@Component({
    selector: 'app-edit-attendee-profile',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './edit_attendee_profile.component.html',
    styleUrls: ['./edit_attendee_profile.component.scss']
})
export class EditAttendeeProfileComponent implements AfterViewInit, OnDestroy {
    editForm: FormGroup;
    attendee: EditAttendeeTourProfileModel;
    meansOfTransportation$: Observable<MeanOfTransportationModel[]>;
    autoCompleteItems$: Observable<PlaceModel[]>;
    constructor(
        private fb: FormBuilder,
        private attendeesService: AttendeesService,
        private restService: DataService,
        private router: Router,
        private locationService: LocationApiService,
        private notify: NotificationService
    ) {
        this.editForm = this.fb.group(new EditAttendeeTourProfileForm({
            id: null,
            isBuildOwnTour: false,
            isJoinOtherTour: false,
            isPress: false,
            isOrga: false,
            home: {
                latitude: null,
                longitude: null,
                name: null,
            },
            daysToParticipate: 0,
            distanceToParticipate: 0,
            meanOfTransportation: 0,
            unit: 'km'
        }));
    }

    ngAfterViewInit() {
        this.attendeesService.getCurrentTourAttendeeProfile().subscribe(attendee => {
            this.attendee = attendee;
            this.editForm.patchValue(attendee);
            if (attendee.meanOfTransportation && attendee.meanOfTransportation.hasOwnProperty('id')) {
                const transportation = attendee.meanOfTransportation as MeanOfTransportationModel;
                this.editForm.get('meanOfTransportation').patchValue(transportation.id);
            }
        });
        this.meansOfTransportation$ = this.restService.getMeanOfTransportation();
        ModalService.on('div.modal.registration');
    }

    ngOnDestroy(): void {
        ModalService.off();
    }

    onSelectPoint(item: PlaceModel): void {
        this.editForm.get('home').patchValue({name: item.display_name, latitude: item.lat, longitude: item.lon});
        this.autoCompleteItems$ = null;
    }

    onKeyUpname($event: any): void {
        const value = $event.target.value;
        if (value.length < 3) {
            return;
        }

        this.autoCompleteItems$ = this.locationService.getProposals(value);
    }


    onClose(): void {
        ModalService.off();
    }

    onSubmit(): void {
        const attendee: EditAttendeeTourProfileModel = {
            id: this.editForm.get('id').value,
            isJoinOtherTour: this.editForm.get('isJoinOtherTour').value,
            isBuildOwnTour: this.editForm.get('isBuildOwnTour').value,
            isPress: this.editForm.get('isPress').value,
            isOrga: this.editForm.get('isOrga').value,
            home: this.editForm.get('home').value,
            daysToParticipate: this.editForm.get('daysToParticipate').value,
            distanceToParticipate: this.editForm.get('distanceToParticipate').value,
            meanOfTransportation: this.editForm.get('meanOfTransportation').value,
            unit: this.editForm.get('unit').value,
        };

        this.attendeesService.updateTourProfile(attendee).subscribe(() => {
            this.notify.success('Updated tour profile.', 'Success', true);
            this.router.navigate(['']);
        }, (err: HttpError) => {
            let message = '';
            err.errors.forEach(msg =>  {
                message += msg;
            });
            this.notify.error(message, err.title);
        });
    }
}
