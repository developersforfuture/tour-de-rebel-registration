import { Component } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-logout',
  template: `
        <mat-card>
          <mat-card-header>
            <mat-card-title><p>You really want to logout?</p></mat-card-title>
          </mat-card-header>
          <mat-card-actions>
            <button mat-raised-button color="primary" (click)="onSubmit()">Logout</button>
            <button mat-raised-button color="warn" (click)="onClose()">Close</button>
          </mat-card-actions>
        </mat-card>
  `
})
export class LogoutComponent {
  loginForm: FormBuilder;
  constructor(private authenticationservice: AuthenticationService, private router: Router) {}

  onSubmit(): void {
    this.authenticationservice.logout();
    location.href = '/';
  }

  onClose(): void {
    this.router.navigate(['']);
  }
}
