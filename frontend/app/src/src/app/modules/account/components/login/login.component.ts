import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { ModalService } from 'src/app/shared/services';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private authenticationservice: AuthenticationService,
    private router: Router,
    private notify: NotificationService
    ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get invalid(): boolean {
    return this.passwordControlInvalid || this.usernameControlInvalid;
  }

  get passwordControl(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }

  get passwordConfirmControl(): FormControl {
    return this.loginForm.get('passwordConfirm') as FormControl;
  }

  get usernameControl(): FormControl {
    return this.loginForm.get('username') as FormControl;
  }

  get passwordControlInvalid(): boolean {
    return this.passwordControl.hasError('required')
      && this.passwordControl.touched;
  }

  get usernameControlInvalid(): boolean {
    return this.usernameControl.hasError('required') && this.usernameControl.touched;
  }

  ngOnInit(): void {
    this.authenticationservice.currentUserToken.subscribe(t => {
      if (null !== t) {
        this.router.navigate(['account']);
      }
    });
    ModalService.on('.modal.login');
  }

  ngOnDestroy(): void {
    ModalService.off();
  }

  onSubmit(): void {
    const username = this.usernameControl.value;
    const password = this.passwordControl.value;
    this.authenticationservice.login(username, password).subscribe(() => {
      this.notify.success('Successfully logged in.', 'Success', true);
      ModalService.off();
      this.router.navigate(['']);
    }, (err) => {
      const message = err.hasOwnProperty('message') ? err.message + ': ' : '';
      this.notify.error(message, err.title);
        });
  }

  onClose(): void {
    ModalService.off();
    this.router.navigate(['']);
  }
}
