import { Component, OnInit, Output, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EditAttendeePasswordModel } from '../../models/edit_attendee_password.model';
import { AttendeesService } from '../../services/attendees.service';
import { CustomValidators } from 'src/app/modules/account/form/validator/custom-validators';
import { ModalService } from 'src/app/shared/services';
import { HttpError } from 'src/app/shared/models/http_error.mdel';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';

@Component({
    selector: 'app-edit-attendee-password',
    templateUrl: './edit_attendee_password.component.html',
    styleUrls: ['./edit_attendee_password.component.scss']
})
export class EditAttendeePasswordComponent implements AfterViewInit, OnDestroy {
    editForm: FormGroup;
    attendee: EditAttendeePasswordModel;
    constructor(
        private fb: FormBuilder,
        private attendeesService: AttendeesService,
        private router: Router,
        private notify: NotificationService
    ) {
        this.editForm = this.fb.group({
            oldPlainPassword: [null, Validators.required],
            password: [null, Validators.compose([
                Validators.required,
                CustomValidators.patternValidator(/\d/, { hasNumber: true }),
                CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
                CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
                CustomValidators.patternValidator(/[@$!%*#?&]/, {hasSpecialCharacters: true}),
                Validators.minLength(8),
            ])],
            passwordConfirm: [null, Validators.compose([Validators.required])],
        },
        {
            validator: CustomValidators.passwordMatchValidator
        });
    }
    get invalid(): boolean {
        return this.passwordConfirmControlInvalid || this.passwordControlInvalid || this.oldPasswordControlInvalid;
    }

    get oldPasswordControl(): FormControl {
        return this.editForm.get('oldPlainPassword') as FormControl;
    }

    get oldPasswordControlInvalid(): boolean {
        return this.oldPasswordControl.touched && this.oldPasswordControl.invalid;
    }

    get passwordControl(): FormControl {
        return this.editForm.get('password') as FormControl;
    }

    get passwordConfirmControl(): FormControl {
        return this.editForm.get('passwordConfirm') as FormControl;
    }

    get passwordHasMinLength(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('minlength');
    }

    get passwordHasNumber(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasNumber');
    }

    get passwordHasCapitalCase(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasCapitalCase');
    }

    get passwordHasSmallCase(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasSmallCase');
    }

    get passwordHasSpecialCharacters(): boolean {
        return this.passwordControl.hasError('required') || this.passwordControl.hasError('hasSpecialCharacters');
    }

    get passwordControlInvalid(): boolean {
        return this.passwordControl.touched && this.passwordControl.invalid;
    }

    get passwordConfirmControlInvalid(): boolean {
        return this.passwordConfirmControl.touched && this.passwordConfirmControl.invalid;
    }

    ngAfterViewInit() {
        ModalService.on('div.modal.registration');
    }

    ngOnDestroy(): void {
        ModalService.off();
    }

    onClose(): void {
        ModalService.off();
    }

    onSubmit(): void {
        const attendee: EditAttendeePasswordModel = {
            oldPlainPassword: this.oldPasswordControl.value,
            plainPassword: this.passwordControl.value,
        };

        this.attendeesService.updateTourAttendeePassword(attendee).subscribe(() => {
            this.notify.success('Updated password.', 'Success', true);
            this.router.navigate(['']);
        }, (err: HttpError) => {
            let message = '';
            err.errors.forEach(msg =>  {
                message += msg;
            });
            this.notify.error(message, err.title);
        });
    }
}
