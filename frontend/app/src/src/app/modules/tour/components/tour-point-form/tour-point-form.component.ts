import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LocationApiService } from 'src/app/shared/services';
import { PlaceModel } from 'src/app/modules/map/models/place.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tour-point-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './tour-point-form.component.html',
  styleUrls: ['./tour-point-form.component.scss']
})
export class TourPointFormComponent implements OnInit {
  @Input() tourPointForm: FormGroup;
  @Input() name: string;
  autoCompleteItems$: Observable<PlaceModel[]>;
  longitude: number;
  latitude: number;

  constructor(private locationService: LocationApiService) { }
  ngOnInit(): void {
    this.latitude = this.tourPointForm.get('latitude').value;
    this.longitude = this.tourPointForm.get('longitude').value;
  }
  onSelectPoint(item: PlaceModel): void {
    const currentLatitudeControl = this.tourPointForm.get('latitude') as FormControl;
    const currentLongitudeControl = this.tourPointForm.get('longitude') as FormControl;
    const currentNameControl = this.tourPointForm.get('name') as FormControl;
    this.latitude = Number(item.lat);
    this.longitude = Number(item.lon);
    currentLatitudeControl.setValue(item.lat);
    currentLongitudeControl.setValue(item.lon);
    currentNameControl.setValue(item.display_name);
    this.autoCompleteItems$ = null;
  }

  onKeyUpname($event: any): void {
    const value = $event.target.value;
    if (value.length < 3) {
      return;
    }

    this.autoCompleteItems$ = this.locationService.getProposals(value);
  }

}
