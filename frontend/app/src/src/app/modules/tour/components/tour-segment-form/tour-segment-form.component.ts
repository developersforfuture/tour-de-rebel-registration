import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormGroupName } from '@angular/forms';

@Component({
  selector: 'app-tour-segment-form',
  templateUrl: './tour-segment-form.component.html',
  styleUrls: ['./tour-segment-form.component.scss']
})
export class TourSegmentFormComponent implements OnInit {
  @Input() tourSegmentForm: FormGroup;
  @Input() index: number;
  start: FormGroup;
  end: FormGroup;
  constructor() { }

  ngOnInit(): void {
    this.start = this.tourSegmentForm.get('start') as FormGroup;
    this.end = this.tourSegmentForm.get('end') as FormGroup;
  }
}
