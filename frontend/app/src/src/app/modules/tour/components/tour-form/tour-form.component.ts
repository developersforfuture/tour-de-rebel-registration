import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormGroupName } from '@angular/forms';
import { Subscription, BehaviorSubject } from 'rxjs';
import { TourFormService } from '../../service/tour-form.service';
import { TourModel, TourType, TourState } from '../../models/tour.model';
import { AppState } from 'src/app/store';
import { Store } from '@ngrx/store';
import { UpdateTour, CreateTour, RemoveTour } from 'src/app/store/actions/tours.action';

@Component({
  selector: 'app-tour-form',
  templateUrl: './tour-form.component.html',
  styleUrls: ['./tour-form.component.scss']
})
export class TourFormComponent implements OnInit {
  @Input() tour: TourModel;
  tourForm: FormGroup;
  tourFormSub: Subscription;
  formInvalid = false;
  tourSegments: FormArray;
  start: FormGroup;
  end: FormGroup;
  organizer: FormGroup;
  types = [TourType.BIKE, TourType.WALK, TourType.SCOOTER, TourType.RIDE_HORSE];
  states = [TourState.DRAFT, TourState.INTERNAL, TourState.PUBLISHED, TourState.VERIFIED];

  constructor(private tourFormService: TourFormService, private store: Store<AppState>) { }

  ngOnInit(): void {
    if (this.tour) {
      this.tourFormService.init(this.tour);
      this.tourFormSub = this.tourFormService.tourForm$.subscribe(form => {
        this.tourForm = form;
        this.tourSegments = this.tourForm.get('tourSegments') as FormArray;
        this.organizer = this.tourForm.get('organizer') as FormGroup;
        this.start = this.tourForm.get('start') as FormGroup;
        this.end = this.tourForm.get('end') as FormGroup;
      });
    }
  }

  addSegment() {
    this.tourFormService.addSegment();
  }

  deleteSegment(index: number): void {
    this.tourFormService.deleteSegment(index);
  }

  updateTour(): void {
    this.store.dispatch(new UpdateTour(this.tourFormService.getModelData()));
  }

  createTour(): void {
    this.store.dispatch(new CreateTour(this.tourFormService.getModelData()));
  }

  deleteTour(): void {
    this.store.dispatch(new RemoveTour(this.tour));
  }
}
