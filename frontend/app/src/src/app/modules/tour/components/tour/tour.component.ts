import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { getAllTours } from 'src/app/store/selectors/tour.selectors';
import { Observable } from 'rxjs';
import { TourModel } from '../../models/tour.model';
import { LoadTours } from 'src/app/store/actions/tours.action';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {
  tours$: Observable<TourModel[]>;
  formGroup: FormGroup;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.tours$ = this.store.pipe(select(getAllTours));
    this.store.dispatch(new LoadTours());
  }
}
