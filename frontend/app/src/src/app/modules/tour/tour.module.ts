import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TourComponent } from './components/tour/tour.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import {
  MatCardModule,
  MatListModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatSelectModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatInputModule} from '@angular/material';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TourFormComponent } from './components/tour-form/tour-form.component';
import { OrganizerFormComponent } from './components/organizer-form/organizer-form.component';
import { TourPointFormComponent } from './components/tour-point-form/tour-point-form.component';
import { TourSegmentFormComponent } from './components/tour-segment-form/tour-segment-form.component';

export const TOUR_ROUTES: Routes = [
  { path: 'dashboard', component: TourComponent }
];


@NgModule({
  declarations: [TourComponent, TourFormComponent, OrganizerFormComponent, TourPointFormComponent, TourSegmentFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(TOUR_ROUTES),
    SharedModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSelectModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
  ]
})
export class TourModule { }
