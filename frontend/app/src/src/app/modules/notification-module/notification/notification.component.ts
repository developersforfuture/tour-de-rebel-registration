import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationService } from '../notification.service';
import { NotificationType } from '../notification_message.model';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
      this.subscription = this.notificationService.getMessage().subscribe(message => {
          this.message = message;
      });
  }

  onClose(): void {
    this.notificationService.close();
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

  getClassByType(type: string): string {
    switch (type) {
      case NotificationType.ERROR:
        return 'alert alert-error';
      case NotificationType.INFO:
        return 'alert alert-info';
      case NotificationType.SUCCESS:
        return 'alert alert-success';
      case NotificationType.WARN:
        return 'alert alert-warn';
    }

    return 'alert';
  }

}
