import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttendeeDonationComponent } from '../donations/attendee-donation/attendee-donation.component';
import { AddAttendeeDonationComponent } from '../donations/add-attendee-donation/add-attendee-donation.component';
import { CounterComponent } from './counter/counter.component';
import {
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatInput,
  MatInputModule,
  MatListModule,
  MatBadgeModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatDatepickerModule,
  ShowOnDirtyErrorStateMatcher,
  ErrorStateMatcher,
  MatSelectModule
} from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';

const DONATION_ROUTES: Routes = [
  { path: 'list', component: AttendeeDonationComponent},
  { path: 'add-donation', component: AddAttendeeDonationComponent },
];

@NgModule({
  declarations: [
    AttendeeDonationComponent,
    AddAttendeeDonationComponent,
    CounterComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatInputModule,
    MatListModule,
    MatBadgeModule,
    MatSelectModule,
    MatExpansionModule,
    RouterModule.forChild(DONATION_ROUTES),
    FlexLayoutModule,
    MatButtonToggleModule
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  exports: [CounterComponent]
})
export class DonationsModule { }
