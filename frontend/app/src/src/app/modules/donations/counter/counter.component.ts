import { Component, OnInit } from '@angular/core';
import { Unit, TourAttendeeDonation } from '../tour_attendee_donation.model';
import { Store } from '@ngrx/store';
import { AppState, getAllAttendeeDonations } from 'src/app/store';
import { Observable } from 'rxjs';
import { CustomIconService } from 'src/app/shared/services/custom-icon.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  donations$: Observable<TourAttendeeDonation[]>;
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.donations$ = this.store.select(getAllAttendeeDonations);
  }

  unit(donations: TourAttendeeDonation[]): string {
    let unit: Unit;
    donations.forEach(d => {
      unit = d.unit;
    });

    return unit;
  }
  sum(donations: TourAttendeeDonation[]): number {
    if (!donations) {
      return 0;
    }

    return donations.reduce((sum, donation) => {
      sum += donation.amount;
      return sum;
    }, 0);
  }
}
