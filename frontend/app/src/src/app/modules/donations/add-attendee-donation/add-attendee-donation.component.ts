import { Component, OnInit } from '@angular/core';
import { ModalService, LocationApiService, DataService } from 'src/app/shared/services';
import { Store, Action } from '@ngrx/store';
import { AppState, AddDonationAction } from 'src/app/store';
import { Observable } from 'rxjs';
import { TourAttendeeDonation, Unit } from '../tour_attendee_donation.model';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { PlaceModel } from '../../map/models/place.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { AddAttendeeDonation, ADD_ATTENDEE_DONATION_SUCCESS, ADD_ATTENDEE_DONATION_FAIL } from 'src/app/store/actions/attendee.action';
import { ErrorStateMatcher } from '@angular/material';
import * as moment from 'moment';
import { AttendeesService } from '../../account/services/attendees.service';
import { ofType, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpError } from 'src/app/shared/models/http_error.mdel';
import { NotificationService } from '../../notification-module/notification.service';
import { iconForMeanOfTransportationIdfunction as iconForMeanOfTransportationId } from 'src/app/shared/services/custom-icon.service';

@Component({
  selector: 'app-add-attendee-donation',
  templateUrl: './add-attendee-donation.component.html',
  styleUrls: ['./add-attendee-donation.component.scss']
})
export class AddAttendeeDonationComponent implements OnInit {
  donations$: Observable<TourAttendeeDonation[]>;
  donationForm: FormGroup;
  meansOfTransportation$: Observable<MeanOfTransportationModel[]>;
  autoCompleteItems$: Observable<PlaceModel[]>;
  forceEditBasics = false;
  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder,
    private locationService: LocationApiService,
    private dataService: DataService,
    private attendeeService: AttendeesService,
    private notify: NotificationService,
    private router: Router,
    updates$: Actions
  ) {
    updates$.pipe(ofType(ADD_ATTENDEE_DONATION_SUCCESS)).subscribe((action: AddAttendeeDonation) => {
      this.store.dispatch(new AddDonationAction(action.payload));
      this.notify.success('Created new Donation.', 'Success', true);
      this.router.navigate(['']).then(() => {
        location.reload(true);
      });
    });
    updates$.pipe(ofType(ADD_ATTENDEE_DONATION_FAIL)).subscribe(e => {
      const err = e as HttpError;
      let message =  '';
      err.errors.forEach(msg =>  {
          message += msg;
      });
      this.notify.error(message, err.title);
    });

    this.donationForm = this.fb.group({
      point: this.fb.group({
        latitude: [null, Validators.compose([Validators.required])],
        longitude: [null, Validators.required],
        name: [null, Validators.required]
      }),
      date: [null, Validators.required],
      amount: [null, Validators.compose([Validators.required])],
      by: [null, Validators.required],
      unit: [null, Validators.required]
    });
  }

  forceEdit(): boolean {
    this.forceEditBasics = true;
    return false;
  }


  iconByMeaningOfTransportation(key: number): string {
    return iconForMeanOfTransportationId(key);
  }

  get editBasics(): boolean {
    if (null === this.donationForm || this.forceEditBasics) {
      return true;
    }

    return !this.donationForm.get('point').value || !this.donationForm.get('unit').value || !this.donationForm.get('by').value;
  }

  ngOnInit(): void {
    this.meansOfTransportation$ = this.dataService.getMeanOfTransportation();
    this.amountControl.patchValue(0);
    this.unitControl.patchValue(Unit.KM);
    this.dateControl.patchValue(moment().format('DD/MM/YYYY'));

    this.attendeeService.getCurrentTourAttendeeProfile().subscribe(attendee => {
        this.donationForm.get('point').patchValue(attendee.home);
        this.unitControl.patchValue(attendee.unit);
        if (attendee.meanOfTransportation && attendee.meanOfTransportation.hasOwnProperty('id')) {
          const transportation = attendee.meanOfTransportation as MeanOfTransportationModel;
          this.donationForm.get('by').patchValue(transportation.id);
        }
    });
  }

  onSubmit(): boolean {
    const donation: TourAttendeeDonation = {
      id: null,
      by: this.donationForm.get('by').value,
      point: {
        latitude: parseFloat(this.donationForm.get('point').value.latitude),
        longitude: parseFloat(this.donationForm.get('point').value.longitude),
        name: this.donationForm.get('point').value.name
      },
      date: this.dateControl.value,
      amount: this.donationForm.get('amount').value,
      unit: this.donationForm.get('unit').value
    };

    this.store.dispatch(new AddAttendeeDonation(donation));
    return false;
  }
  get invalid(): boolean {
    return (this.byControl.touched && this.byControl.hasError('required'))
      || (this.amountControl.touched && this.amountControl.hasError('required'))
      || this.nameControlInvalid;
  }
  get unitControl(): FormControl {
    return this.donationForm.get('unit') as FormControl;
  }
  get byControl(): FormControl {
    return this.donationForm.get('by') as FormControl;
  }
  get amountControl(): FormControl {
    return this.donationForm.get('amount') as FormControl;
  }
  get pointControl(): FormGroup {
    return this.donationForm.get('point') as FormGroup;
  }
  get pointNameControl(): FormControl {
    return this.pointControl.get('name') as FormControl;
  }

  get pointLatitudeControl(): FormControl {
    return this.pointControl.get('latitude') as FormControl;
  }

  get pointLongitudeControl(): FormControl {
    return this.pointControl.get('longitude') as FormControl;
  }

  get dateControl(): FormControl {
    return this.donationForm.get('date') as FormControl;
  }

  get pointInvalid(): boolean {
    return this.pointNameControl.touched && this.pointNameControl.invalid;
  }

  get nameControlInvalid(): boolean {
    return this.pointNameControl.touched && this.pointNameControl.hasError('required');
  }

  onSelectPoint(item: PlaceModel): void {
      this.donationForm.get('point').patchValue({name: item.display_name, latitude: item.lat, longitude: item.lon});
      this.autoCompleteItems$ = null;
  }

  onKeyUpname($event: any): void {
    const value = $event.target.value;
    if (value.length < 3) {
      return;
    }
    this.autoCompleteItems$ = this.locationService.getProposals(value);
  }

  onClose(): void {
    ModalService.off();
  }
}
