import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TourAttendeeDonation } from './tour_attendee_donation.model';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DonationsService {

  constructor(private http: HttpClient) { }

  getAttendeeDonations(): Observable<TourAttendeeDonation[]> {
    return this.http
      .get<TourAttendeeDonation[]>(`${environment.backendUrl}/tour-management/tourattendees/donations`)
      .pipe(catchError((error: any) => {
        return Observable.throw(error);
      }));
  }

  addAttendeeDonation(donation: TourAttendeeDonation): Observable<TourAttendeeDonation> {
    return this.http
      .post<TourAttendeeDonation>(`${environment.backendUrl}/tour-management/tourattendees/donations`, donation)
      .pipe(catchError((error: any) => {
        return Observable.throw(error);
      }));
  }
}
