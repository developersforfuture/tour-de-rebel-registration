import { TourPoint } from 'src/app/modules/tour/models/tour_point.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { Point } from '../tour/models/point.model';

export enum Unit {
    KM = 'km',
    M = '',
}

export interface TourAttendeeDonation {
    id: number;
    point: Point;
    date: string;
    amount: number;
    by: MeanOfTransportationModel;
    unit: Unit.KM | Unit.M;
}
