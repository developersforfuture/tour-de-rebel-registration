import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppState, getAllAttendeeDonations } from 'src/app/store';
import { Store } from '@ngrx/store';
import { TourAttendeeDonation, Unit } from '../tour_attendee_donation.model';
import { ModalService } from 'src/app/shared/services';
import { FormBuilder } from '@angular/forms';
import { iconForMeanOfTransportationIdfunction } from 'src/app/shared/services/custom-icon.service';

@Component({
  selector: 'app-attendee-donation',
  templateUrl: './attendee-donation.component.html',
  styleUrls: ['./attendee-donation.component.scss']
})
export class AttendeeDonationComponent implements OnInit, OnDestroy, AfterViewInit {

  donation: TourAttendeeDonation;
  donations$: Observable<TourAttendeeDonation[]>;
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.donations$ = this.store.select(getAllAttendeeDonations);
  }

  ngAfterViewInit() {
    ModalService.on('div.modal.donations');
  }

  ngOnDestroy(): void {
    ModalService.off();
  }

  iconByMeaningOfTransportation(key: number): string {
    return iconForMeanOfTransportationIdfunction(key);
  }

}
