import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../shared/services';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { OpenlayersMapService } from './services/openlayers_map.service';
import { TourModel } from '../tour/models/tour.model';
import { Store } from '@ngrx/store';
import { AppState, EventSelectedAction, TourSelectedAction, SegmentSelectedAction, LoadMapAction } from 'src/app/store';
import { getAllToursOnMap, getAllEventsOnMap, getAllDonationsOnMap } from 'src/app/store/selectors/map_data.selectors';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() zoom: number;
  @Input() latitude: number;
  @Input() longitude: number;
  eventsInPopup: TourEvent[] = [];
  segmentsInPopup: TourSegmentModel[] = [];
  toursInPopup: TourModel[] = [];
  constructor(
    private dataService: DataService,
    private olMap: OpenlayersMapService,
    private store: Store<AppState>
    ) { }

  ngOnInit(): void {
    this.olMap.initMap(
      {
        zoom: this.zoom,
        latitude: this.latitude,
        longitude: this.longitude,
        popup: 'popup',
        map: 'map',
        controls: 'controls',
        clusterDistance: 40,
      }
    );

    this.store.dispatch(new LoadMapAction());
    this.store.select(getAllToursOnMap).subscribe(tours => {
      tours.forEach(tour => {
        this.olMap.addTourToMap(tour);
      });
    });
    this.store.select(getAllEventsOnMap).subscribe(events => {
      this.olMap.addEventsToMap(events);
    });
    this.store.select(getAllDonationsOnMap).subscribe(donations => {
      this.olMap.addOrUpdateDonationsOnMap(donations);
    });

    this.olMap.onSingleClick((event: {events: TourEvent[], segments: TourSegmentModel[], tours: TourModel[]}) => {
      event.events.forEach(e => this.store.dispatch(new EventSelectedAction(e)));
      event.segments.forEach(s => this.store.dispatch(new SegmentSelectedAction(s)));
      event.tours.forEach(t => this.store.dispatch(new TourSelectedAction(t)));
    });
  }

  getEventTitle(event: TourEvent, short: boolean): string {
    let title = (event.author !== undefined ? event.author + ': ' : '') + event.title + ', ';
    title += (event.point.name ? event.point.name + ', ' : '');
    if (short) {
      return title;
    }
    title += new Date(event.date_from * 1000)
    .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
    if (event.date_to) {
      title += ' - ' + new Date(event.date_to * 1000)
      .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
    }
    return title;
  }

  getSegmentTitle(segment: TourSegmentModel): string {
    let title = '';
    if (segment.start.name || segment.end.name) {
      title += '(' + (segment.start.name ? segment.start.name + ' - ' : '') + segment.end.name + ')';
    }
    if (segment.tour) {
      title += ' - Tour: ' + segment.tour.title;
      if (segment.tour.start.name || segment.tour.end.name) {
        title += ' (' + (segment.tour.start.name ? segment.tour.start.name + ' - ' : '') + segment.tour.end.name + ')';
      }
    }
    return title;
  }
}
