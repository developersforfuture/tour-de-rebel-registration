import { Injectable } from '@angular/core';
import * as ol from 'openlayers';
import { MapConfigModel } from '../map_config.model';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { TourPoint } from 'src/app/modules/tour/models/tour_point.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';
import { TourModel } from '../../tour/models/tour.model';
import { OpenlayerFeatureUtil } from './openlayers/openlayer_feature_util.service';
import { environment } from 'src/environments/environment';
import { OlMapDecoratorService } from './ol-map-decorator.service';
import { TourAttendeeDonation } from '../../donations/tour_attendee_donation.model';

const EVENT_STYLE = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 6,
    fill: new ol.style.Fill({ color: [225, 0, 255, 0.6] })
  })
});

const TOUR_START_END_POINT_STYLE = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 12,
    fill: new ol.style.Fill({ color: [0, 255, 0, 0.5] })
  })
});

const SEGMENT_START_END_POINT_STYLE = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 10,
    fill: new ol.style.Fill({ color: [225, 0, 255, 0.6] })
  })
});
const STROKE_STYLE = new ol.style.Style({
  stroke: new ol.style.Stroke({
    width: 4,
    color: [255, 255, 0, 0.9]
  })
});

const ownTourStyleFunktion = (() => {
  const Image = new ol.style.Circle({
    radius: 5,
    fill: null,
    stroke: new ol.style.Stroke({ color: 'orange', width: 2 })
  });
  const GeoPoint = TOUR_START_END_POINT_STYLE;
  const Polygon = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)'
    })
  });
  const MultiLineString = STROKE_STYLE;
  const LineString = STROKE_STYLE;
  const MultiPolygon = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)'
    })
  });

  const styles = { Point: GeoPoint, Polygon, MultiLineString, MultiPolygon, Image, LineString };
  const defaultStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'red',
      width: 9
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 155, 0, 0.9)'
    })
  });

  return (feature) => {
    return styles[feature.getGeometry().getType()] || defaultStyle;
  };
})();
const eventStyleFunktion = (() => {
  const Image = new ol.style.Circle({
    radius: 5,
    fill: null,
    stroke: new ol.style.Stroke({ color: 'orange', width: 2 })
  });
  const GeoPoint = EVENT_STYLE;
  const Polygon = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)'
    })
  });
  const MultiLineString = STROKE_STYLE;
  const LineString = STROKE_STYLE;
  const MultiPolygon = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)'
    })
  });

  const styles = { Point: GeoPoint, Polygon, MultiLineString, MultiPolygon, Image, LineString };
  const defaultStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'red',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 0, 0, 0.1)'
    })
  });

  return (feature) => {
    return styles[feature.getGeometry().getType()] || defaultStyle;
  };
})();

const styleCache: {[id: string]: ol.style.Style} = {};
const donationStyle = (feature) => {
  const styledFeatures: ol.feature[] = feature.get('features');
  const size = styledFeatures.reduce((sum, f) => sum + f.getProperties().donation.amount, 0);
  let style = styleCache[size];
  if (!style) {
    style = new ol.style.Style({
      image: new ol.style.Circle({
        radius: 20,
        stroke: new ol.style.Stroke({
          color: '#fff'
        }),
        fill: new ol.style.Fill({
          color: '#3399CC'
        })
      }),
      text: new ol.style.Text({
        text: size.toString(),
        fill: new ol.style.Fill({
          color: '#fff'
        })
      })
    });
    styleCache[size] = style;
  }
  return style;
};

@Injectable({
  providedIn: 'root'
})
export class OpenlayersMapService {
  private map: ol.Map;
  private overlay: ol.OverLay;
  private popup: HTMLElement;
  private config: MapConfigModel;
  private sources: {[id: string]: ol.source.Source} = {
    donationCluster: null,
    donationVector: null,
  };
  private layers: {[id: string]: ol.layer.Layer} = {
    donation: null
  };

  constructor() { }

  initMap(config: MapConfigModel): void {
    this.popup = document.getElementById(config.popup);
    const controlsElement = document.getElementById(config.controls);
    this.overlay = new ol.Overlay({ element: this.popup });
    this.config = config;

    // const toursvectorLayer = new ol.layer.Vector({
    //   source: new ol.source.Vector({
    //     url: environment.apiUrl + '/tours',
    //     format: new ol.format.GeoJSON(),
    //   }),
    //   title: 'All Tours',
    //   style: tourStyleFunktion,
    //   zIndex: '100'
    // });

    // const eventsVectorLayer = new ol.layer.Vector({
    //   source: new ol.source.Vector({
    //     url: environment.apiUrl + '/events',
    //     format: new ol.format.GeoJSON(),
    //   }),
    //   title: 'All Events',
    //   style: eventStyleFunktion,
    //   zIndex: '100'
    // });

    // const ownToursvectorLayer = new ol.layer.Vector({
    //   source: new ol.source.Vector({
    //     url: environment.apiUrl + '/own-tours',
    //     format: new ol.format.GeoJSON(),
    //   }),
    //   title: 'Own Tours',
    //   style: tourStyleFunktion,
    //   zIndex: '100'
    // });
    const layers = [
      new ol.layer.Tile({ source: new ol.source.OSM(), zIndex: '10' }),
    //   new ol.source.TileJSON({
    //   url: 'https://api.maptiler.com/maps/basic/tiles.json?key=JLPrvRnjkOcAKqetIfeL',
    //   tileSize: 512,
    //   crossOrigin: 'anonymous'
    // })
    ];
    const overlays = [this.overlay];
    this.map = new ol.Map({
      interactions: ol.interaction.defaults({
        altShiftDragRotate: true,
        pinchRotate: false,
        mouseWheelZoom: false,
      }),
      layers,
      overlays,
      target: config.map,
      view: new ol.View({
        center: ol.proj.fromLonLat([config.longitude, config.latitude]),
        zoom: config.zoom
      })
    });

    this.map.addControl(new ol.control.ZoomSlider());
    this.map.addControl(new ol.control.OverviewMap({ layers, target: controlsElement }));
  }

  onSingleClick(fn: CallableFunction) {
    this.map.on('singleclick', (event: ol.events.BaseEvent) => {
      const usedEvents: TourEvent[] = [];
      const usedTourSegments: TourSegmentModel[] = [];
      const usedTours: TourModel[] = [];
      const pixel = this.map.getEventPixel(event.originalEvent);
      this.map.forEachFeatureAtPixel(pixel, (feature, layer) => {
        const properties: any = feature.getProperties();
        if (!properties.hasOwnProperty('type')) {
          return;
        }
        const type = properties.type;
        if ('Event' === type && properties.hasOwnProperty('event')) {
          usedEvents.push(properties.event);
        } else if (('SegmentStart' === type || 'SegmentEnd' === type) && properties.hasOwnProperty('segment')) {
          usedTourSegments.push(properties.segment);
        } else if (('TourStart' === type || 'TourEnd' === type) && properties.hasOwnProperty('tour')) {
          usedTours.push(properties.tour);
        }
      }, {hitTolerance: 100 });
      fn({ events: usedEvents, segments: usedTourSegments, tours: usedTours });
    });
  }
  addEventsToMap(events: TourEvent[]): void {
    const features: ol.feature[] = [];
    events.forEach(event => {
      features.push(OpenlayerFeatureUtil.buildFeatureFromEventPoint(event.point, {type: 'Event', event}));
    });

    const groups = new ol.layer.Vector({
      source: new ol.source.Vector({ features }),
      title: 'Events & Actions',
      zIndex: '100'
    });
    this.map.addLayer(groups);
  }
  addOwnTourToMap(tour: TourModel): void {
    const features: ol.feature[] = OpenlayerFeatureUtil.buildFeatuersFromTour(tour);

    const groups = new ol.layer.Vector({
      source: new ol.source.Vector({ features }),
      title: tour.title,
      zIndex: '100',
      style: ownTourStyleFunktion
    });
    this.map.addLayer(groups);
  }

  addTourToMap(tour: TourModel): void {
    const features: ol.feature[] = OpenlayerFeatureUtil.buildFeatuersFromTour(tour);

    const groups = new ol.layer.Vector({
      source: new ol.source.Vector({ features }),
      title: tour.title,
      zIndex: '100'
    });
    this.map.addLayer(groups);
  }



  addOrUpdateDonationsOnMap(donations: TourAttendeeDonation[]): void {
    if (this.layers.donation === null) {
      const features = OpenlayerFeatureUtil.buildFeatuersFromDonations(donations);
      this.sources.donationVector = new ol.source.Vector({features});
      this.sources.donationCluster = new ol.source.Cluster({distance: this.config.clusterDistance, source: this.sources.donationVector});
      this.layers.donation = new ol.layer.Vector({
        source: this.sources.donationCluster,
        title: 'All Donations',
        zIndex: '1000',
        style: donationStyle
      });
      this.map.addLayer(this.layers.donation);
    } else {
      const newDonations: TourAttendeeDonation[] = [];
      const existingDonations = this.sources.donationVector.getFeatures().reduce((acc, f) => {
        return {...acc, [f.getProperties().donation.id]: f.getProperties().donation};
      }, {});
      const newDonationEntities = donations.reduce((acc, d) => {
        return { ...acc, [d.id]: d};
      }, {});
      for (const key in newDonationEntities) {
        if (!existingDonations.hasOwnProperty(key)) {
          newDonations.push(newDonationEntities[key]);
        }
      }
      const features = OpenlayerFeatureUtil.buildFeatuersFromDonations(newDonations);
      this.sources.donationVector.addFeatures(features);
      this.map.updateSize();
    }
  }
}
