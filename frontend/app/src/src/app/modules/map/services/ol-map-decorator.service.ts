import { Injectable } from '@angular/core';
import * as ol from 'openlayers';
import { MapConfigModel } from '../map_config.model';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { TourPoint } from 'src/app/modules/tour/models/tour_point.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';
import { TourModel } from '../../tour/models/tour.model';

const EVENT_STYLE = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 6,
    fill: new ol.style.Fill({ color: [225, 0, 255,] })
  })
});

const TOUR_START_END_POINT_STYLE = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 6,
    fill: new ol.style.Fill({ color: [0, 0, 255] })
  })
});

const SEGMENT_START_END_POINT_STYLE = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 6,
    fill: new ol.style.Fill({ color: [225, 0, 255, 0.6] })
  })
});
const STROKE_STYLE = new ol.style.Style({
  stroke: new ol.style.Stroke({
    width: 3,
    color: [255, 0, 0, 1]
  })
});

const styleFunction = (() => {
  const image = new ol.style.Circle({
      radius: 5,
      fill: null,
      stroke: new ol.style.Stroke({ color: 'orange', width: 2 })
    });
  const GeoPoint = TOUR_START_END_POINT_STYLE;
  const Polygon = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)'
    })
  });
  const MultiLineString = STROKE_STYLE;
  const MultiPolygon = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)'
    })
  });

  const styles = {Point: GeoPoint, Polygon, MultiLineString, MultiPolygon, image};
  const defaultStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'red',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 0, 0, 0.1)'
    })
  });

  return (feature) => {
    return styles[feature.getGeometry().getType()] || defaultStyle;
  };
})();
@Injectable({
  providedIn: 'root'
})
export class OlMapDecoratorService {
  private map: ol.Map;
  private overlay: ol.OverLay;
  private popup: HTMLElement;
  private source: ol.source.OSM;
  private events: { [id: string]: TourEvent } = {};
  private tourSegments: { [id: string]: TourSegmentModel } = {};
  private points: { [id: string]: TourPoint } = {};
  private vectorLayer: ol.layer.Vector;

  constructor() { }

  private buildPointFeature(point: TourPoint): ol.feature {
    const feature = new ol.Feature(
      new ol.geom.Point(ol.prjoj.fromLongLat([point.longitude, point.latitude]))
    );
    feature.setId('point_' + point.id);

    return feature;
  }

  private buildLineFeature(pointFrom: TourPoint, pointTo: TourPoint): ol.feature {
    const feature = new ol.Feature(
      new ol.geom.LineString([
        ol.prjoj.fromLongLat([pointFrom.longitude, pointFrom.latitude]),
        ol.prjoj.fromLongLat([pointTo.longitude, pointTo.latitude])
      ])
    );
    feature.setId('line_' + pointFrom.id + '_' + pointTo.id);

    return feature;
  }


  initMap(config: MapConfigModel): void {
    this.source = new ol.source.OSM();
    this.popup = document.getElementById(config.popup);
    this.overlay = new ol.Overlay({ element: this.popup });

    const features = [];
    this.vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({ features }),
      style: styleFunction,
    });

    const layers = [
      new ol.layer.Tile({ source: this.source }),
      this.vectorLayer
    ];

    this.map = new ol.Map({
      controls: ol.control.defaults().extend([
        new ol.control.FullScreen(),
        new ol.control.OverviewMap({
          layers: [
            new ol.layer.Tile({ source: this.source })
          ]
        }),
        new ol.control.ScaleLine({ minWidth: 120 }),
      ]),
      interactions: ol.interaction.defaults({
        altShiftDragRotate: true,
        pinchRotate: false
      }).extend([
        new ol.interaction.DragRotateAndZoom()
      ]),
      layers,
      overlays: [this.overlay],
      target: config.map,
      view: new ol.View({
        center: ol.prjoj.fromLongLat([config.longitude, config.latitude]),
        zoom: config.zoom
      })
    });

    // todo: move them back to component, where it belongs to.
    this.onPointerMove((pixel, hit) => {
      document.getElementById(this.map.getTarget()).style.cursor = hit ? 'pointer' : '';
    });
  }

  onSingleClick(fn: CallableFunction) {
    this.map.on('singleclick', (event: ol.events.BaseEvent) => {
      const usedEvents = [];
      const usedTourSegments = [];
      this.map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
        const id: string = feature.getId();
        if (id === undefined) {
          return;
        }

        if (this.events[id]) {
          usedEvents.push(this.events[id]);
          return;
        } else if (this.points[id]) {
          const [domain, loc, entryId] = id.split(/\_/);

          if (this.tourSegments[domain + '_' + entryId]) {
            usedTourSegments.push(this.tourSegments[domain + '_' + entryId]);
            return;
          }
        }
      }, { hitTolerance: 10 });
      usedTourSegments.sort((x, y) => x - y);
      usedEvents.sort((x, y) => x - y);

      fn({ events: usedEvents, segments: usedTourSegments });
    });
  }

  onPointerMove(fn: CallableFunction): void {
    this.map.on('pointermove', (event: ol.events.PointerEvent) => {
      const pixel = this.map.getEventPixel(event.originalEvent);
      const hit = this.map.hasFeatureAtPixel(pixel, { hitTolerance: 10 });
      fn(pixel, hit);
    });
  }

  addEventsToMap(events: TourEvent[]): void {
    const features: ol.feature[] = [];
    for (const event of events) {
      features.push(this.buildPointFeature(event.point));
    }

    const vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({ features }),
      title: 'Events',
      style: EVENT_STYLE
    });
    this.map.addLayer(vectorLayer);
  }

  addTourToMap(tour: TourModel): void {
    const features: ol.feature[] = [];
    features.push(this.buildPointFeature(tour.start));
    features.push(this.buildPointFeature(tour.end));
    for (const tourSegment of tour.tourSegments) {
      features.push(this.buildLineFeature(tourSegment.start, tourSegment.end));
      features.push(this.buildPointFeature(tourSegment.start));
      features.push(this.buildPointFeature(tourSegment.end));
    }

    const vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({ features }),
      title: tour.title,
      style: styleFunction
    });
    this.map.addLayer(vectorLayer);
  }
}
