import * as ol from 'openlayers';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { TourPoint } from 'src/app/modules/tour/models/tour_point.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';
import { TourModel } from '../../../tour/models/tour.model';
import { TourAttendeeDonation } from 'src/app/modules/donations/tour_attendee_donation.model';
import { Point } from 'src/app/modules/tour/models/point.model';

const EVENT_STYLE = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({ color: [0, 225, 255, 0.8] })
    })
});

const TOUR_START_END_POINT_STYLE = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 20,
        fill: new ol.style.Fill({ color: [255, 0, 200, 1] })
    })
});

const DONATION_POINT_STYLE = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 20,
        fill: new ol.style.Fill({ color: [255, 0, 200, 1] })
    })
});

const SEGMENT_START_END_POINT_STYLE = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({ color: [225, 0, 255, 0.8] })
    })
});
const STROKE_STYLE = new ol.style.Style({
    stroke: new ol.style.Stroke({
        width: 4,
        color: [255, 255, 0, 0.9]
    })
});

export class OpenlayerFeatureUtil {
    static buildFeatureFromSegmentPoint(point: TourPoint, properties: {type: string, segment: TourSegmentModel}): ol.feature {
        const feature = new ol.Feature(
            new ol.geom.Point(ol.proj.fromLonLat([point.longitude, point.latitude]))
        );
        feature.setId('point_' + point.id);
        feature.setStyle(SEGMENT_START_END_POINT_STYLE);
        feature.setProperties(properties);

        return feature;
    }

    static buildFeatureFromEventPoint(point: TourPoint, properties: {type: string, event: TourEvent}): ol.feature {
        const feature = new ol.Feature(
            new ol.geom.Point(ol.proj.fromLonLat([point.longitude, point.latitude]))
        );
        feature.setId('segment_point_' + point.id);
        feature.setStyle(EVENT_STYLE);
        feature.setProperties(properties);

        return feature;
    }

    static buildFeatureFromTourPoint(point: TourPoint, properties: {type: string, tour: TourModel}): ol.feature {
        const feature = new ol.Feature(
            new ol.geom.Point(ol.proj.fromLonLat([point.longitude, point.latitude]))
        );
        feature.setId('tour_point_' + point.id);
        feature.setStyle(TOUR_START_END_POINT_STYLE);
        feature.setProperties(properties);

        return feature;
    }

    static buildFeatureFromDonationPoint(
        point: Point,
        properties: {type: string, donation: TourAttendeeDonation}
    ): ol.feature {
        const feature = new ol.Feature(
            new ol.geom.Point(ol.proj.fromLonLat([point.longitude, point.latitude]))
        );
        feature.setStyle(DONATION_POINT_STYLE);
        feature.setProperties(properties);

        return feature;
    }

    static buildFeatureFromSegmentLine(from: TourPoint, to: TourPoint, properties: {type: string, segment: TourSegmentModel}): ol.feature {
        const feature = new ol.Feature(
            new ol.geom.LineString([
                ol.proj.fromLonLat([from.longitude, from.latitude]),
                ol.proj.fromLonLat([to.longitude, to.latitude])
            ])
        );
        feature.setId('segment_line_' + from.id + '_' + to.id);
        feature.setStyle(STROKE_STYLE);
        feature.setProperties(properties);

        return feature;
    }

    static buildFeaturesFromTourSegment(segment: TourSegmentModel): ol.feature[] {
        const features = [];
        features.push(OpenlayerFeatureUtil.buildFeatureFromSegmentPoint(segment.start, {type: 'SegmentStart', segment}));
        features.push(OpenlayerFeatureUtil.buildFeatureFromSegmentPoint(segment.end, {type: 'SegmentStart', segment}));
        features.push(OpenlayerFeatureUtil.buildFeatureFromSegmentLine(segment.start, segment.end, {type: 'Segment', segment})
            );

        return features;
    }

    static buildFeatuersFromTour(tour: TourModel): ol.feature[] {
        const features: ol.feature = [];
        features.push(OpenlayerFeatureUtil.buildFeatureFromTourPoint(tour.start, {type: 'TourStart', tour}));
        features.push(OpenlayerFeatureUtil.buildFeatureFromTourPoint(tour.end, {type: 'TourEnd', tour})
        );
        tour.tourSegments.forEach(segment => {
            OpenlayerFeatureUtil.buildFeaturesFromTourSegment(segment).forEach(f => {
                features.push(f);
            });
        });

        return features;
    }

    static buildFeatuersFromDonations(donations: TourAttendeeDonation[]): ol.feature[] {
        const features: ol.feature = [];
        donations.forEach(donation => {
            const feature = OpenlayerFeatureUtil.buildFeatureFromDonationPoint(donation.point, {type: 'Donation', donation});
            feature.setId('donation_' + donation.id);
            feature.setStyle(DONATION_POINT_STYLE);
            features.push(feature);
        });

        return features;
    }
}
