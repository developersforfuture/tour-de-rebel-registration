import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MapModel } from '../models/map.model';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MapDataService {

  constructor(private $http: HttpClient) { }

  getMapData(): Observable<MapModel> {
    return this
      .$http
      .get<MapModel>(environment.backendUrl + '/data/map ')
      .pipe(catchError((error: any) => throwError(error.json)))
      ;
  }
}
