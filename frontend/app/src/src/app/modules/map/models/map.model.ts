import { TourEvent } from '../../tour/models/tour_event.model';
import { TourModel } from '../../tour/models/tour.model';
import { TourAttendeeDonation } from '../../donations/tour_attendee_donation.model';

export interface MapModel {
    events: TourEvent[];
    tours: TourModel[];
    donations: TourAttendeeDonation[];
}
