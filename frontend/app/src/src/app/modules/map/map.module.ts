import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OlMapDecoratorService } from './services/ol-map-decorator.service';
import { TourModule } from '../tour/tour.module';
import { OpenlayersMapService } from './services/openlayers_map.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { MapOverlayComponent } from './component/map-overlay/map-overlay.component';
import { MapOverlayTourComponent } from './component/map-overlay-tour/map-overlay-tour.component';
import { MapOverlayTourSegmentComponent } from './component/map-overlay-tour-segment/map-overlay-tour-segment.component';
import { MapOverlayTourEventComponent } from './component/map-overlay-tour-event/map-overlay-tour-event.component';
import { MatListModule, MatCardModule } from '@angular/material';

@NgModule({
  declarations: [MapComponent, MapOverlayComponent, MapOverlayTourComponent, MapOverlayTourSegmentComponent, MapOverlayTourEventComponent],
  imports: [
    CommonModule,
    TourModule,
    SharedModule,
    HttpClientModule,
    HttpModule,
    MatListModule,
    MatCardModule,
  ],
  providers: [OlMapDecoratorService, OpenlayersMapService],
  exports: [MapComponent]
})
export class MapModule { }
