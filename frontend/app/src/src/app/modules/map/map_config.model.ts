export interface MapConfigModel {
    latitude: number;
    longitude: number;
    zoom: number;
    popup: string;
    controls: string;
    map: string;
    clusterDistance: number;
}
