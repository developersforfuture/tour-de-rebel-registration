import { Component, OnInit, Input } from '@angular/core';
import { TourSegmentFormComponent } from 'src/app/modules/tour/components/tour-segment-form/tour-segment-form.component';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';

@Component({
  selector: 'app-map-overlay-tour-segment',
  templateUrl: './map-overlay-tour-segment.component.html',
  styleUrls: ['./map-overlay-tour-segment.component.scss']
})
export class MapOverlayTourSegmentComponent implements OnInit {
  @Input() segment: TourSegmentModel;
  constructor() { }

  ngOnInit(): void {
  }

}
