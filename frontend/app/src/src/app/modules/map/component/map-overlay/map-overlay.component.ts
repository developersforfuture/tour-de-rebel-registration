import { Component, OnInit, Input } from '@angular/core';
import { TourModel } from 'src/app/modules/tour/models/tour.model';
import { TourSegmentModel } from 'src/app/modules/tour/models/tour_segment.model';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';
import { AppState, getSelectedTours, getSelectedEvents, getSelectedSegments } from 'src/app/store';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-map-overlay',
  templateUrl: './map-overlay.component.html',
  styleUrls: ['./map-overlay.component.scss']
})
export class MapOverlayComponent implements OnInit {

  tours$: Observable<TourModel[]>;
  segments$: Observable<TourSegmentModel[]>;
  events$: Observable<TourEvent[]>;
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.tours$ = this.store.pipe(select(getSelectedTours));
    this.events$ = this.store.pipe(select(getSelectedEvents));
    this.segments$ = this.store.pipe(select(getSelectedSegments));
  }

}
