import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapOverlayTourComponent } from './map-overlay-tour.component';

describe('MapOverlayTourComponent', () => {
  let component: MapOverlayTourComponent;
  let fixture: ComponentFixture<MapOverlayTourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapOverlayTourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapOverlayTourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
