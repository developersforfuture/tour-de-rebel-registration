import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapOverlayTourEventComponent } from './map-overlay-tour-event.component';

describe('MapOverlayTourEventComponent', () => {
  let component: MapOverlayTourEventComponent;
  let fixture: ComponentFixture<MapOverlayTourEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapOverlayTourEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapOverlayTourEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
