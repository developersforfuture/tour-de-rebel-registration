import { Component, OnInit, Input } from '@angular/core';
import { TourEvent } from 'src/app/modules/tour/models/tour_event.model';

@Component({
  selector: 'app-map-overlay-tour-event',
  templateUrl: './map-overlay-tour-event.component.html',
  styleUrls: ['./map-overlay-tour-event.component.scss']
})
export class MapOverlayTourEventComponent implements OnInit {
  @Input() event: TourEvent;
  constructor() { }

  ngOnInit(): void {
  }

}
