import { Component, OnInit, Input } from '@angular/core';
import { TourModel } from 'src/app/modules/tour/models/tour.model';

@Component({
  selector: 'app-map-overlay-tour',
  templateUrl: './map-overlay-tour.component.html',
  styleUrls: ['./map-overlay-tour.component.scss']
})
export class MapOverlayTourComponent implements OnInit {
  @Input() tour: TourModel;
  constructor() { }

  ngOnInit(): void {
  }

}
