import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapOverlayTourSegmentComponent } from './map-overlay-tour-segment.component';

describe('MapOverlayTourSegmentComponent', () => {
  let component: MapOverlayTourSegmentComponent;
  let fixture: ComponentFixture<MapOverlayTourSegmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapOverlayTourSegmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapOverlayTourSegmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
