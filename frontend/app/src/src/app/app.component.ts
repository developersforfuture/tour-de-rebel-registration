import { Component, Input, OnInit } from '@angular/core';
import { ConfigService } from './shared/services';

@Component({
  selector: 'app-root',
  template: `
    <app-default></app-default>
  `
})
export class AppComponent implements OnInit {
  @Input() config: {[id: string]: string};

  constructor(private configService: ConfigService) {}

  ngOnInit(): void {
    this.configService.setConfig(this.config);
  }
}
