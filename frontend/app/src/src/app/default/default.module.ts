import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { DefaultComponent } from './default/default.component';
import { AccountModule } from 'src/app/modules/account/account.module';
import { TourModule } from 'src/app/modules/tour/tour.module';
import { MapModule } from 'src/app/modules/map/map.module';
import { EventModule } from 'src/app/modules/event/event.module';
import { RouterModule, Routes } from '@angular/router';
import { MatSidenavModule} from '@angular/material';
import { FlexLayoutModule} from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from 'src/app/shared/components/page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AuthGuard } from 'src/app/shared/helpers';
import { RegistrationFormComponent } from '../modules/account/components/registration/registration.component';
import { SidenavGuard } from '../shared/helpers/sidenav.guard';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';

import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { environment } from '../../environments/environment';
export const metaReducers: MetaReducer<any>[] = !environment.production
    ? [storeFreeze]
    : [];

import { reducers, effects } from '../store';
import { LoginComponent } from '../modules/account/components/login/login.component';
import { NotificationModule } from '../modules/notification-module/notification.module';

export const DEFAULT_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [SidenavGuard],
    data: {sidenav: false}
  },
  {
    path: 'events',
    loadChildren: './../modules/event/event.module#EventModule',
    canActivate: [AuthGuard, SidenavGuard],
    data: {sidenav: true}
  },
  {
    path: 'tours',
    loadChildren: './../modules/tour/tour.module#TourModule',
    canActivate: [AuthGuard, SidenavGuard],
    data: {sidenav: true}
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [SidenavGuard],
    data: {sidenav: true}
  },
  {
    path: 'register',
    component: RegistrationFormComponent,
    canActivate: [SidenavGuard],
    data: {sidenav: true}
  },
  {
    path: 'account',
    loadChildren: './../modules/account/account.module#AccountModule',
    canActivate: [AuthGuard, SidenavGuard],
    data: {sidenav: true}
  },
  {
    path: 'donations',
    loadChildren: './../modules/donations/donations.module#DonationsModule',
    canActivate: [AuthGuard, SidenavGuard],
    data: {sidenav: true}
  },
  {
    path: '**',
    component: PageNotFoundComponent,
    canActivate: [SidenavGuard],
    data: {sidenav: true}
  }
];


@NgModule({
  declarations: [DefaultComponent, HomeComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(DEFAULT_ROUTES, {useHash: true}),
    SharedModule,
    AccountModule,
    TourModule,
    MapModule,
    EventModule,
    MatSidenavModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    StoreModule.forRoot({}, { metaReducers }),
    StoreModule.forFeature('app', reducers),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot(effects),
    NotificationModule,
  ],
  exports: [DefaultComponent]
})
export class DefaultModule { }
