import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/modules/account/services/authentication.service';
import { User } from 'src/app/modules/account/models/user.model';
import { Store } from '@ngrx/store';
import * as fromStore from './../../store';
import { LoadAttendeeDonationAction } from './../../store';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';
import { CustomIconService } from 'src/app/shared/services/custom-icon.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  title = 'TdP - Management';
  user$: Observable<User>;
  sidenavVisible: boolean;

  public constructor(
      private authService: AuthenticationService,
      private store: Store<fromStore.AppState>,
      private notify: NotificationService,
      private customIconService: CustomIconService
  ) {}

  ngOnInit(): void {
    this.user$ = this.authService.currentUser;
    this.user$.subscribe(user => {
      if (null !== user) {
        this.store.dispatch(new LoadAttendeeDonationAction());
      } else {
        this.notify.info('Please login or register to edit your own climate friendly movements');
      }
    });
    this.store.select(fromStore.getSiteNavigationVisibility).subscribe((state: boolean) => {
      this.sidenavVisible = state;
    });
    this.customIconService.init();
  }
}
