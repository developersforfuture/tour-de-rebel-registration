export interface HttpError {
    type: string;
    code?: number;
    title: string;
    errors: string[];
}
