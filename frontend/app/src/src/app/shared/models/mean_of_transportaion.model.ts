export class MeanOfTransportationModel {
    id: number;
    name: string;
    shortName: string;
}
