import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable,  } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/modules/account/services/authentication.service';
import { Router } from '@angular/router';
import { HttpError } from '../models/http_error.mdel';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                if (!this.router.url.match(/login/)) {
                    location.reload(true);
                }
            }
            const error: HttpError = {
                type: '',
                code: err.status,
                title: 'Unknown',
                errors: [],
            }
            if (err.status === 500) {
                error.title = 'Server Error';
                error.type = 'server_error';
            } else if (err.status === 502) {
                error.title = 'Server under pressure - please try again later';
                error.type = 'server_error';
            } else  if (err.status === 404) {
                error.title = 'Not found - invalid router configuration.';
                error.type = 'app_error';
            } else if (err.hasOwnProperty('message')) {
                error.title = err.message;
            } else if (err.hasOwnProperty('statuText')) {
                error.title = err.statuText;
            }

            if (err.hasOwnProperty('error')) {
                if (err.error.hasOwnProperty('type')) {
                    error.type = err.error.type;
                }
                if (err.error.hasOwnProperty('title')) {
                    error.title = err.error.title;
                }
                if (err.error.hasOwnProperty('erors')) {
                    error.errors = err.error.errors;
                }
            }

            return Observable.throw(error);
        }));
    }
}
