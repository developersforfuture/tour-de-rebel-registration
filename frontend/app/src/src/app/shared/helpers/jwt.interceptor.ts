import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from 'src/app/modules/account/services/authentication.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUserToken = this.authenticationService.currentUserTokenValue;
        if (currentUserToken && currentUserToken.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUserToken.token}`
                }
            });
        }

        return next.handle(request);
    }
}
