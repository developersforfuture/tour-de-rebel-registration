import { Component, Input, OnInit } from '@angular/core';
import { User, UserToken } from 'src/app/modules/account/models/user.model';
import { AuthenticationService } from 'src/app/modules/account/services/authentication.service';
import { CustomIconService } from '../../services/custom-icon.service';

@Component({
  selector: 'app-navigation',
  templateUrl: 'navigation.component.html'
})
export class NavigationComponent implements OnInit {
  @Input() user: User;
  @Input() title: string;

  constructor(private authService: AuthenticationService, private iconService: CustomIconService) {}

  get isUser(): boolean {
    return !!(this.user);
  }

  ngOnInit(): void {
    this.authService.currentUser.subscribe(user => {
      this.user = user;
    });
    this.iconService.init();
  }
}
