import { DataService } from './data.service';
import { ModalService } from './modal.service';
import { LocationApiService } from './location_api.service';
import { ConfigService } from './config.service';


export const services: any[] = [
    DataService,
    ModalService,
    LocationApiService,
    ConfigService
];

export * from './data.service';
export * from './modal.service';
export * from './location_api.service';
export * from './config.service';
