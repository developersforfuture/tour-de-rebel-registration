import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { MapModel } from 'src/app/modules/map/models/map.model';
import { Country } from '../models/country.model';
import { MeanOfTransportationModel } from '../models/mean_of_transportaion.model';
import { Organisation } from '../models/organisation.model';
import { ConfigService } from './config.service';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private $http: HttpClient) { }

  getOrganisations(): Observable<Organisation[]> {
    return this.$http.get<Organisation[]>(environment.backendUrl + '/data/organisations')
    .pipe(catchError((error: any) => throwError(error.json)));
  }

  getCountries(): Observable<Country[]> {
    return this.$http.get<Country[]>(environment.backendUrl + '/data/countries')
    .pipe(catchError((error: any) => throwError(error.json)));
  }

  getMeanOfTransportation(): Observable<MeanOfTransportationModel[]> {
    return this.$http.get<MeanOfTransportationModel[]>(environment.backendUrl + '/data/meanoftransportations')
    .pipe(catchError((error: any) => throwError(error.json)));
  }
}
