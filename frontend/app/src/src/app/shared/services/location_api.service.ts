import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { PlaceModel } from 'src/app/modules/map/models/place.model';

@Injectable()
export class LocationApiService {
    constructor(private $http: HttpClient) {}

    getProposals(search: string): Observable<PlaceModel[]> {
        return this.$http.get<PlaceModel[]>(environment.locationApiUrl + 'search?q=' + search + '&format=json')
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }
}
