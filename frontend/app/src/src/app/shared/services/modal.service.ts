import * as $ from 'jquery';

function _window(): any {
    // return the global native browser window object
    return window;
}

export class ModalService {
    get nativeWindow(): any {
        return _window();
    }
    static on(selector: string = 'div.modal') {
        _window().scrollTo({ left: 0, top: 100, behavior: 'smooth' });
        $(selector).show();

    }

    static off(selector: string = 'div.modal') {
        $(selector).hide();
    }
}
