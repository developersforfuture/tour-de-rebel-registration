import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

export enum Icon {
  BOAT = 'boat',
  CROSS = 'cross',
  DELETE = 'delete',
  DONATED_KM = 'donated_km',
  EVENT_PEACE_SIGN = 'event_peace_sign',
  FLAG = 'flag',
  HIKING_BOOT = 'hiking_boot',
  HORSE = 'horse',
  LOCATION = 'location',
  MINUS = 'minus',
  MY_EVENTS_PEACE_SIGN = 'my_events_peace_sign',
  MY_EVENTS = 'my_events',
  MY_KM = 'my_km',
  MY_TOURS = 'my_tours',
  NOMAL_BIKE = 'normal_bike',
  PLUS = 'plus',
  RACE_BIKE = 'race_bike',
  RUNNING_SHOE = 'running_shoe',
  SKATEBOARD = 'skateboard'
}

const icons: string[] = [
  Icon.BOAT,
  Icon.CROSS,
  Icon.DELETE,
  Icon.DONATED_KM,
  Icon.EVENT_PEACE_SIGN,
  Icon.FLAG,
  Icon.HIKING_BOOT,
  Icon.HORSE,
  Icon.LOCATION,
  Icon.MINUS,
  Icon.MY_EVENTS_PEACE_SIGN,
  Icon.MY_EVENTS,
  Icon.MY_KM,
  Icon.MY_TOURS,
  Icon.NOMAL_BIKE,
  Icon.PLUS,
  Icon.RACE_BIKE,
  Icon.RUNNING_SHOE,
  Icon.SKATEBOARD,
];


export const iconForMeanOfTransportationIdfunction = (id: number): string => {
  switch (id) {
    case 1:
      return 'tdp_' + Icon.NOMAL_BIKE;
    case 2:
      return 'tdp_' + Icon.NOMAL_BIKE;
    case 3:
      return 'tdp_' + Icon.NOMAL_BIKE;
    case 4:
      return 'tdp_' + Icon.HIKING_BOOT;
    case 5:
      return 'tdp_' + Icon.RUNNING_SHOE;
    case 6:
      return 'tdp_' + Icon.BOAT;
    case 7:
      return 'tdp_' + Icon.RACE_BIKE;
    case 8:
      return 'tdp_' + Icon.SKATEBOARD;
  }

  return 'tdp_' + Icon.NOMAL_BIKE;
};

@Injectable({
  providedIn: 'root'
})
export class CustomIconService {

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {}
  init() {
    icons.forEach(icon => {
      this.matIconRegistry.addSvgIcon(
        'tdp_' + icon,
        this.domSanitizer.bypassSecurityTrustResourceUrl('../../assets/icons/' + icon + '.svg')
      );
    });
  }
}
