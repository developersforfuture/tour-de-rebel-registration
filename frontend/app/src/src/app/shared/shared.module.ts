import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataService, LocationApiService, ModalService } from './services';
import { NavigationComponent } from './components/navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from '../modules/account/services/authentication.service';
import { AttendeesService } from '../modules/account/services/attendees.service';
import { FooterComponent } from './components/footer/footer.component';
import {
  MatDividerModule,
  MatToolbarModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatCommonModule,
  MatCardModule,
  MatListModule,
  MatSidenavModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JwtInterceptor, ErrorInterceptor } from './helpers';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DonationsModule } from '../modules/donations/donations.module';
import { TransportationDirective } from './directives/transportation.directive';


@NgModule({
  declarations: [
    NavigationComponent,
    FooterComponent,
    PageNotFoundComponent,
    TransportationDirective,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatCommonModule,
    MatDividerModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    FlexLayoutModule,
    HttpClientModule,
    MatCardModule,
    DonationsModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
  ],
  providers: [
    AttendeesService,
    AuthenticationService,
    DataService,
    LocationApiService,
    ModalService,
    MatDividerModule,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  exports: [NavigationComponent, FooterComponent]
})
export class SharedModule { }
