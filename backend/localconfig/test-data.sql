INSERT INTO "organisation" (id,name,short_name) values (2,'Keine Organisations', '');
INSERT INTO "organisation" (id,name,short_name) values (0,'Fridays For Future', 'FFF');
INSERT INTO "organisation" (id,name,short_name) values (1,'Extinction Rebelion', 'XR');

INSERT INTO "country" (id,name,short_name) values (0,'Deutschland', 'DE');
INSERT INTO "country" (id,name,short_name) values (1,'Polen', 'PL');
INSERT INTO "country" (id,name,short_name) values (2,'Frankreich', 'FR');
INSERT INTO "country" (id,name,short_name) values (3,'England', 'GB');
INSERT INTO "country" (id,name,short_name) values (4,'Dänemark', 'DE');
INSERT INTO "country" (id,name,short_name) values (5,'Deutschland', 'DK');

INSERT INTO "attendees" (id, username, password, first_name, second_name, mail,organisation_id,is_cycling,is_press,is_orga,country_id)
values (1000, 'test', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'First Name', 'Second Name',
        'a@b.de',0,true,false,false,1);

INSERT INTO "attendees" (id, username, password, first_name, second_name, mail,organisation_id,is_cycling,is_press,is_orga,country_id)
values (1001, 'test2', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'First Name 2', 'Second Name 2',
        'c@d.de',1,false,true,false,2);

INSERT INTO "team" (id,name,description,leader_id) values (1000,'Main tolles Team 1','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1001,'Main tolles Team 2','epische beschreibung',1001);
INSERT INTO "team" (id,name,description,leader_id) values (1002,'Main tolles Team 3','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1003,'Main tolles Team 4','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1004,'Main tolles Team 5','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1005,'Main tolles Team 6','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1006,'Main tolles Team 7','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1007,'Main tolles Team 8','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1008,'Main tolles Team 9','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1009,'Main tolles Team 10','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1010,'Main tolles Team 11','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1011,'Main tolles Team 12','epische beschreibung',1000);

INSERT INTO "team_members" (team_id,members_id) values (1000,1000);
INSERT INTO "team_members" (team_id,members_id) values (1000,1001);
INSERT INTO "team_members" (team_id,members_id) values (1001,1001);
INSERT INTO "team_members" (team_id,members_id) values (1002,1000);
INSERT INTO "team_members" (team_id,members_id) values (1003,1000);
INSERT INTO "team_members" (team_id,members_id) values (1004,1000);
INSERT INTO "team_members" (team_id,members_id) values (1005,1000);
INSERT INTO "team_members" (team_id,members_id) values (1006,1000);
INSERT INTO "team_members" (team_id,members_id) values (1007,1000);
INSERT INTO "team_members" (team_id,members_id) values (1008,1000);
INSERT INTO "team_members" (team_id,members_id) values (1009,1000);
INSERT INTO "team_members" (team_id,members_id) values (1010,1000);
INSERT INTO "team_members" (team_id,members_id) values (1011,1000);

INSERT INTO "tour" (id,name,description,is_public,owner_id) values (1000,'Meine Tour','Epische Tourbeschreibung',true,1000);

INSERT INTO "attendees_tours" (attendees_id,tours_id) values (1000,1000);

INSERT INTO "point" (id,latitude,longitude,tour_id) values (1000,1.010101,2.10101010,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1001,1.010101,2.10101011,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1002,1.010101,2.10101012,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1003,1.010101,2.10101013,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1004,1.010101,2.10101014,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1005,1.010101,2.10101015,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1006,1.010101,2.10101016,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1007,1.010101,2.10101017,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1008,1.010101,2.10101018,1000);
INSERT INTO "point" (id,latitude,longitude,tour_id) values (1009,1.010101,2.10101019,1000);

INSERT INTO "tour_points" (tour_id,points_id) values (1000,1000);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1001);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1002);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1003);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1004);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1005);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1006);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1007);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1008);
INSERT INTO "tour_points" (tour_id,points_id) values (1000,1009);

INSERT INTO "tour_segment" (id,name,start_point_id,end_point_id,tour_id) values(1000,'Teil 1',1001,1003,1000);
INSERT INTO "tour_segment" (id,name,start_point_id,end_point_id,tour_id) values(1001,'Teil 2',1003,1008,1000);

INSERT INTO "tour_tour_segments" (tour_id,tour_segments_id) values (1000,1000);
INSERT INTO "tour_tour_segments" (tour_id,tour_segments_id) values (1000,1001);