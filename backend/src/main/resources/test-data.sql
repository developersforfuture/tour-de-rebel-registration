INSERT INTO "organisation" (id,name,short_name) values (0,'Fridays For Future', 'FFF');
INSERT INTO "organisation" (id,name,short_name) values (1,'Extinction Rebelion', 'XR');
INSERT INTO "organisation" (id,name,short_name) values (2,'Keine Organisation', '');

INSERT INTO "mean_of_transportation" (id,name,middle_daily_distance) values (0,'No selection ...', 0);
INSERT INTO "mean_of_transportation" (id,name,middle_daily_distance) values (1,'Bike', 30);
INSERT INTO "mean_of_transportation" (id,name,middle_daily_distance) values (2,'Scooter', 20);
INSERT INTO "mean_of_transportation" (id,name,middle_daily_distance) values (3,'Walking', 10);

INSERT INTO "mean_of_transportation" (id,name) values (0,'No selection ...');
INSERT INTO "mean_of_transportation" (id,name) values (1,'Bike');
INSERT INTO "mean_of_transportation" (id,name) values (2,'Scooter');
INSERT INTO "mean_of_transportation" (id,name) values (3,'Walking');

INSERT INTO "country" (id,name,short_name) values (0,'Deutschland', 'DE');
INSERT INTO "country" (id,name,short_name) values (1,'Polen', 'PL');
INSERT INTO "country" (id,name,short_name) values (2,'Frankreich', 'FR');
INSERT INTO "country" (id,name,short_name) values (3,'England', 'GB');
INSERT INTO "country" (id,name,short_name) values (4,'Dänemark', 'DE');
INSERT INTO "country" (id,name,short_name) values (5,'Deutschland', 'DK');

INSERT INTO "attendees" (id, username, password, first_name, second_name, mail,organisation_id,is_join_other_tour, is_build_own_tour,is_press,is_orga,country_id,days_to_participate,distance_to_participate)
values (1000, 'test', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'First Name', 'Second Name',
        'a@b.de',0,true,false,false,false,1,0,0);

INSERT INTO "attendees" (id, username, password, first_name, second_name, mail,organisation_id,is_join_other_tour, is_build_own_tour,is_press,is_orga,country_id,days_to_participate,distance_to_participate)
values (1001, 'test2', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'First Name 2', 'Second Name 2',
        'c@d.de',1,false,true,false,false,2,0,0);

INSERT INTO "team" (id,name,description,leader_id) values (1000,'Main tolles Team 1','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1001,'Main tolles Team 2','epische beschreibung',1001);
INSERT INTO "team" (id,name,description,leader_id) values (1002,'Main tolles Team 3','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1003,'Main tolles Team 4','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1004,'Main tolles Team 5','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1005,'Main tolles Team 6','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1006,'Main tolles Team 7','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1007,'Main tolles Team 8','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1008,'Main tolles Team 9','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1009,'Main tolles Team 10','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1010,'Main tolles Team 11','epische beschreibung',1000);
INSERT INTO "team" (id,name,description,leader_id) values (1011,'Main tolles Team 12','epische beschreibung',1000);

INSERT INTO "team_members" (team_id,members_id) values (1000,1000);
INSERT INTO "team_members" (team_id,members_id) values (1000,1001);
INSERT INTO "team_members" (team_id,members_id) values (1001,1001);
INSERT INTO "team_members" (team_id,members_id) values (1002,1000);
INSERT INTO "team_members" (team_id,members_id) values (1003,1000);
INSERT INTO "team_members" (team_id,members_id) values (1004,1000);
INSERT INTO "team_members" (team_id,members_id) values (1005,1000);
INSERT INTO "team_members" (team_id,members_id) values (1006,1000);
INSERT INTO "team_members" (team_id,members_id) values (1007,1000);
INSERT INTO "team_members" (team_id,members_id) values (1008,1000);
INSERT INTO "team_members" (team_id,members_id) values (1009,1000);
INSERT INTO "team_members" (team_id,members_id) values (1010,1000);
INSERT INTO "team_members" (team_id,members_id) values (1011,1000);
