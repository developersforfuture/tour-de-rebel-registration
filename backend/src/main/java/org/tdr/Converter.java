package org.tdr;

import org.tdr.dto.*;
import org.tdr.model.*;
import org.tdr.service.converter.TourConverter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Converter {

    public static EditAttendeesDTO convertAttendeesToEditAttendeesDTO(Attendees attendees) {
        return EditAttendeesDTO.builder()
                .id(attendees.getId())
                .username(attendees.getUsername())
                .mail(attendees.getMail())
                .firstName(attendees.getFirstName())
                .secondName(attendees.getSecondName())
            .build();
    }

    public static EditAttendeesProfileDTO convertAttendeesToEditAttendeesProfileDTO(Attendees attendees) {
        return EditAttendeesProfileDTO.builder()
                    .id(attendees.getId())
                    .description(attendees.getDescription())
                    .startPoint(TourConverter.convertTourPointToTourPointDTO(attendees.getStartPoint()))
                    .daysToParticipate(attendees.getDaysToParticipate())
                    .distanceToParticipate(attendees.getDistanceToParticipate())
                    .isJoinOtherTour(attendees.getIsJoinOtherTour())
                    .isBuildOwnTour(attendees.getIsBuildOwnTour())
                    .isOrga(attendees.getIsOrga())
                    .isPress(attendees.getIsPress())
                .build();
    }

    public static EditAttendeesPasswordDTO convertAttendeesToEditAttendeesPasswordDTO(Attendees attendees) {
        return EditAttendeesPasswordDTO.builder()
                .id(attendees.getId())
                .build();
    }

    public static OrganisationDTO convertOrganistationToOrganistationDTO(Organisation organisation) {
        return OrganisationDTO.builder().id(organisation.getId()).name(organisation.getName()).shortName(organisation.getShortName()).build();
    }

    public static List<OrganisationDTO> convertOrganistationsToOrganistationDTOs(Collection<Organisation> organisations) {
        return organisations.stream().map(Converter::convertOrganistationToOrganistationDTO).collect(Collectors.toList());
    }

    public static CountryDTO convertCountyToCountyDTO(Country country) {
        return CountryDTO.builder().id(country.getId()).name(country.getName()).shortName(country.getShortName()).build();
    }

    public static List<CountryDTO> convertCountiesToCountyDTOs(Collection<Country> countries) {
        return countries.stream().map(Converter::convertCountyToCountyDTO).collect(Collectors.toList());
    }

    public static MeanOfTransportationDTO convertMeanOfTransportationToMeanOfTransportationDTO(MeanOfTransportation meanOfTransportation) {
        return MeanOfTransportationDTO.builder().id(meanOfTransportation.getId()).name(meanOfTransportation.getName()).middleDailyDistance(meanOfTransportation.getMiddleDailyDistance()).build();
    }

    public static List<MeanOfTransportationDTO> convertMeansOfTransportationToMeansOfTransportationDTOs(Collection<MeanOfTransportation> meanOfTransportations) {
        return meanOfTransportations.stream().map(Converter::convertMeanOfTransportationToMeanOfTransportationDTO).collect(Collectors.toList());
    }

    public static TeamDTO convertTeamToTeamDTO(Team team){
            return TeamDTO.builder()
                    .id(team.getId())
                    .name(team.getName())
                    .description(team.getDescription())
                    .leader(convertAttendeesToTeamMemberDTO(team.getLeader()))
                    .members(convertAttendeesetoTeamMemberDTOs(team.getMembers()))
                    .build();
    }

    public static List<TeamDTO> convertTeamsToTeamDTOs(Collection<Team> countries) {
        return countries.stream().map(Converter::convertTeamToTeamDTO).collect(Collectors.toList());
    }

    public static List<TeamMemberDTO> convertAttendeesetoTeamMemberDTOs(Collection<Attendees> countries) {
        return countries.stream().map(Converter::convertAttendeesToTeamMemberDTO).collect(Collectors.toList());
    }

    public static TeamMemberDTO convertAttendeesToTeamMemberDTO(Attendees attendee){
        return TeamMemberDTO.builder()
                .username(attendee.getUsername())
                .id(attendee.getId())
                .firstName(attendee.getFirstName())
                .build();
    }

    public static List<TourDTO> convertToursToTourDTOs(Collection<Tour> tours,boolean withPoints){
        return tours.stream().map(t->convertTourToTourDTO(t,withPoints)).collect(Collectors.toList());
    }


    public static TourDTO convertTourToTourDTO(Tour tour,boolean withPoints){
        return TourDTO.builder()
                .id(tour.getId())
                .name(tour.getName())
                .description(tour.getDescription())
                .owner(convertAttendeesToTourOwnerDTO(tour.getOwner()))
                .points(withPoints?convertPointsToPointDTOs(tour.getPoints()):new ArrayList<>())
                .tourSegments(convertTourSegementsToTourSegementDTOs(tour.getTourSegments()))
                .build();
    }

    public static TourOwnerDTO convertAttendeesToTourOwnerDTO(Attendees attendees){
        return TourOwnerDTO.builder()
                .id(attendees.getId())
                .username(attendees.getUsername())
                .build();
    }

    public static List<PointDTO> convertPointsToPointDTOs(Collection<Point> points){
        return points.stream().map(Converter::convertPoinToPointDTO).collect(Collectors.toList());
    }

    public static PointDTO convertPoinToPointDTO(Point point){
        return PointDTO.builder()
                .id(point.getId())
                .latitude(point.getLatitude())
                .longitude(point.getLongitude())
                .build();
    }

    public static List<TourSegmentDTO> convertTourSegementsToTourSegementDTOs(Collection<TourSegment> points){
        return points.stream().map(Converter::convertTourSegmentToTourSegementDTO).collect(Collectors.toList());
    }

    public static TourSegmentDTO convertTourSegmentToTourSegementDTO(TourSegment tourSegment){
        return TourSegmentDTO.builder()
                .id(tourSegment.getId())
                .name(tourSegment.getName())
                .start(convertPoinToPointDTO(tourSegment.getStartPoint()))
                .end(convertPoinToPointDTO(tourSegment.getEndPoint()))
                .build();
    }

}
