package org.tdr.utils;

import org.springframework.data.domain.Page;
import org.tdr.dto.PageDTO;

import java.util.Collection;
import java.util.List;

public class PageUtils {
    public interface converterInterface<T1,T2>{
        List<T2> convert(Collection<T1> objects);
    }

    public static <T1,T2>  PageDTO<T2> convertPage(Page<T1> pageResult,converterInterface<T1,T2> converterInterface){
        PageDTO<T2> pageDTO = new PageDTO<T2>();
        pageDTO.setTotalElements(pageResult.getTotalElements());
        pageDTO.setTotalPages(pageResult.getTotalPages());
        pageDTO.setPage(pageResult.getNumber()+1);
        pageDTO.setElements(converterInterface.convert(pageResult.getContent()));
        return pageDTO;
    }
}
