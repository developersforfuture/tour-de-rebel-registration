package org.tdr.dto;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class MeanOfTransportationDTO {
    private Long id;
    private String name;
    private Long middleDailyDistance;
}
