package org.tdr.dto;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class EditAttendeesProfileDTO {
    private Long id;
    private String description;
    private TourPointDTO startPoint;
    private Integer daysToParticipate;
    private Integer distanceToParticipate;
    private Integer meanOfTransportation;
    private Boolean isJoinOtherTour;
    private Boolean isBuildOwnTour;
    private Boolean isPress;
    private Boolean isOrga;
}
