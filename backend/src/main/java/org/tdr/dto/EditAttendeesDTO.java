package org.tdr.dto;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class EditAttendeesDTO {
    private Long id;
    private String username;
    private String mail;
    private String firstName;
    private String secondName;
    private Long organisation;
    private Long country;
}
