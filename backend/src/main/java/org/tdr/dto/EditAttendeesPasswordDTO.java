package org.tdr.dto;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class EditAttendeesPasswordDTO {
    private Long id;
    private String formerPassword;
    private String password;
    private String passwordConfirm;
}
