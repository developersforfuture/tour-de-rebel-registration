package org.tdr.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamDTO {
    private Long id;

    private String name;

    private String description;

    private List<TeamMemberDTO> members;

    private TeamMemberDTO leader;
}
