package org.tdr.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PointDTO {
    private Long id;
    private Double latitude;
    private Double longitude;
}
