package org.tdr.dto;

import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Setter
public class MapDTO{
    List<TourDTO> tours;
}
