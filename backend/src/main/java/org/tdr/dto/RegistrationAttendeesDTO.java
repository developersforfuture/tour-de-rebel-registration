package org.tdr.dto;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class RegistrationAttendeesDTO {
    private String username;
    private String password;
    private String mail;
    private String passwordConfirm;
}
