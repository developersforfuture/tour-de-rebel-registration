package org.tdr.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TourPointDTO {
    private Long id;
    private Double latitude;
    private Double longitude;
    private String date;
    private String name;
}
