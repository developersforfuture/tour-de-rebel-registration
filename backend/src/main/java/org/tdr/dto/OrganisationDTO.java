package org.tdr.dto;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class OrganisationDTO {
    private Long id;
    private String name;
    private String shortName;
}
