package org.tdr.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TourOwnerDTO {
    private Long id;
    private String username;
}
