package org.tdr.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TourSegmentDTO {
    private Long id;
    private PointDTO start;
    private PointDTO end;
    private String name;
}
