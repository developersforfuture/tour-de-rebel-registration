package org.tdr.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TourDTO {
    private Long id;
    private String name;
    private String description;
    private List<TourSegmentDTO> tourSegments;
    private List<PointDTO> points;

    private TourOwnerDTO owner;
}
