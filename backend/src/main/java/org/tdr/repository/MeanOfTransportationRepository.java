package org.tdr.repository;

import org.springframework.data.repository.CrudRepository;
import org.tdr.model.MeanOfTransportation;

import java.util.List;

public interface MeanOfTransportationRepository extends CrudRepository<MeanOfTransportation, Long> {

    MeanOfTransportation findById(long id);

    MeanOfTransportation findByName(String username);

    List<MeanOfTransportation> findAll();
}
