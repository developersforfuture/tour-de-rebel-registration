package org.tdr.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.tdr.model.Tour;


public interface TourRepository extends CrudRepository<Tour, Long> {

    Tour findById(long id);

    Tour findByName(String username);

    Page<Tour> findAll();

    Page<Tour> findAllByIsPublic(Boolean isPublic,Pageable pageRequest);

    Page<Tour> findAllByIsPublicAndNameLike(Boolean isPublic, String searchTerm, Pageable pageRequest);
}
getFirstName