package org.tdr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.tdr.dto.TeamDTO;
import org.tdr.model.Attendees;
import org.tdr.model.Team;
import org.tdr.repository.AttendeesRepository;
import org.tdr.repository.TeamRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TeamService {

    @Autowired
    private AttendeesRepository attendeesRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Value("${team.page-size:10}")
    private int pageSize;

    public Team createTeam(TeamDTO teamDTO, Attendees attendees){
        Team team = Team.builder().leader(attendees).description(teamDTO.getDescription()).members(Collections.singletonList(attendees)).name(teamDTO.getName()).build();
        try {
            return teamRepository.save(team);
        } catch(Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Team cannot be created");
        }
    }

    public Team updateTeam(TeamDTO teamDTO, Attendees attendees){
        Team team = teamRepository.findById((long)teamDTO.getId());
        checkExistence(team);
        checkLeadership(attendees, team, "Only the team leader may update the team");
        team.setName(teamDTO.getName());
        team.setDescription(teamDTO.getDescription());
        return teamRepository.save(team);
    }

    public Team getTeam(long id){
        return teamRepository.findById(id);
    }

    public Page<Team> findTeams(String searchTerm,int page){
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        if(StringUtils.isEmpty(searchTerm)){
            return teamRepository.findAll(pageRequest);
        }
        return teamRepository.findAllByNameContaining(searchTerm,pageRequest);
    }

    public Team joinTeam(long id,Attendees attendees){
        Team team = teamRepository.findById(id);
        checkExistence(team);
        if (team.getMembers().stream().anyMatch(v -> Objects.equals(v.getId(), attendees.getId()))) {
            // Idempotent operation: members may join a team again without changing the team
            return team;
        }
        team.getMembers().add(attendees);//TODO replace by only insert command
        return teamRepository.save(team);
    }

    public Team leaveTeam(long id,Attendees attendees){
        Team team = teamRepository.findById(id);
        checkExistence(team);
        if(team.getMembers().stream().noneMatch(v -> Objects.equals(v.getId(), attendees.getId()))){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Attendee is not a team member");
        }
        if(team.getLeader().getId().equals(attendees.getId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The team leader cannot leave the team");
        }
        team.getMembers().removeIf((v)->v.getId().equals(attendees.getId()));//TODO replace by only insert command
        return teamRepository.save(team);
    }

    public Team kickFromTeam(long id,Attendees attendees,long attendeesid){
        Team team = teamRepository.findById(id);
        checkExistence(team);
        checkLeadership(attendees, team, "Only the team leader may kick members form a team");
        if(attendees.getId() == attendeesid){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The team leader cannot be kicked from a team");
        }
        if(team.getMembers().stream().noneMatch(v -> Objects.equals(v.getId(), attendeesid))){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not a team member");
        }
        team.getMembers().removeIf((v)->v.getId().equals(attendeesid));//TODO replace by only insert command
        return teamRepository.save(team);
    }

    public Team makeNewLeader(long id,Attendees attendees,long newLeaderId){
        Team team = teamRepository.findById(id);
        checkExistence(team);
        checkLeadership(attendees, team, "Only the team leader may appoint a new leader");
        if (attendees.getId() == newLeaderId) {
            // Quasi-idempotent operation:  current leader appointing herself again as leader does not change the team
            return team;
        }
        Optional<Attendees> newLeader = team.getMembers().stream().filter(v -> Objects.equals(v.getId(), newLeaderId)).findFirst();
        if(newLeader.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only a team member may become leader");
        }
        team.setLeader(newLeader.get());
        return teamRepository.save(team);
    }

    public List<Team> getTeamsByLeader(Attendees attendees){
        return teamRepository.findAllByLeaderId(attendees.getId());
    }

    /**
     * Checks whether th given attendee is the leader of the given team and throws a {@link ResponseStatusException}
     * with HTTP status 403 if that is not the case.
     */
    private void checkLeadership(Attendees attendees, Team team, String message) {
        if (!Objects.equals(team.getLeader().getId(), attendees.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
        }
    }

    /**
     * Checks whether the given team exists (i.e. is not {@code null}) and throws a {@link ResponseStatusException} with
     * HTTP status 404 if that is not the case.
     * @param team
     */
    private void checkExistence(Team team) {
        if(team == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Team does not exist");
        }
    }
}
