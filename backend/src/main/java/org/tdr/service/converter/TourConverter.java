package org.tdr.service.converter;

import org.tdr.dto.*;
import org.tdr.model.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TourConverter {

    public static MapDTO convertToMapDTO(Collection<Tour> tours) {
        return MapDTO.builder()
                .tours(tours.stream().map(TourConverter::convertTourToTourDTO).collect(Collectors.toList())).build();
    }

    public static TourDTO convertTourToTourDTO(Tour tour) {
        return TourDTO.builder().id(tour.getId()).build();
    }

    public static List<TourDTO> convertToursToTourDTOs(Collection<Tour> tours) {
        return tours.stream().map(TourConverter::convertTourToTourDTO).collect(Collectors.toList());
    }

    public static TourPointDTO convertTourPointToTourPointDTO(TourPoint tourPoint) {
        return TourPointDTO.builder().id(tourPoint.getId()).latitude(tourPoint.getLatitude())
                .longitude(tourPoint.getLongitude()).name(tourPoint.getName()).date(tourPoint.getDate()).build();
    }

    public static List<TourPointDTO> convertTourPointsToTourPointDTOs(Collection<TourPoint> tourPoints) {
        return tourPoints.stream().map(TourConverter::convertTourPointToTourPointDTO).collect(Collectors.toList());
    }

    public static PointDTO convertPointToPointDTO(Point point) {
        return PointDTO.builder().id(point.getId()).build();
    }

    public static List<PointDTO> convertPointsToPointsDTO(Collection<Point> points) {
        return points.stream().map(TourConverter::convertPointToPointDTO).collect(Collectors.toList());
    }

    public static TourSegmentDTO convertTourSegmentToTourSegmentDTO(TourSegment tourSegment) {
        return TourSegmentDTO.builder().id(tourSegment.getId()).build();
    }

    public static List<TourSegmentDTO> convertTourSegmentsToTourSegmentDTOs(Collection<TourSegment> tourSegments) {
        return tourSegments.stream().map(TourConverter::convertTourSegmentToTourSegmentDTO)
                .collect(Collectors.toList());
    }
}
