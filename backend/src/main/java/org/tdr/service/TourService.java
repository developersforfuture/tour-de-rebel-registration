package org.tdr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.tdr.model.Tour;
import org.tdr.repository.TourRepository;

@Service
public class TourService {

    @Value("${team.page-size:10}")
    private int pageSize;

    @Autowired
    private TourRepository tourRepository;

    public Page<Tour> findTeams(String searchTerm, int page){
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        if(StringUtils.isEmpty(searchTerm)){
            return tourRepository.findAllByIsPublic(true,pageRequest);
        }
        return tourRepository.findAllByIsPublicAndNameLike(true,searchTerm,pageRequest);
    }

    public Tour getTour(long id){
        return tourRepository.findById(id);
    }

}
