package org.tdr.service;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import org.tdr.dto.EditAttendeesDTO;
import org.tdr.dto.EditAttendeesProfileDTO;
import org.tdr.dto.EditAttendeesPasswordDTO;
import org.tdr.dto.RegistrationAttendeesDTO;

import org.tdr.model.Attendees;
import org.tdr.model.TourPoint;

import org.tdr.repository.AttendeesRepository;
import org.tdr.repository.CountryRepository;
import org.tdr.repository.MeanOfTransportationRepository;
import org.tdr.repository.OrganisationRepository;
import org.tdr.security.WebSecurityConfig;

@Service
public class AttendeesService {

    @Autowired
    private AttendeesRepository attendeesRepository;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private MeanOfTransportationRepository meanOfTransportationRepository;

    public Attendees createAttendees(RegistrationAttendeesDTO attendeesDTO) {
        if(attendeesRepository.countAllByUsername(attendeesDTO.getUsername())>0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Username already taken");
        }
        if (attendeesRepository.countAllByMail(attendeesDTO.getMail()) > 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already used");
        }

        Attendees attendees = Attendees.builder()
                    .username(attendeesDTO.getUsername())
                    .mail(attendeesDTO.getMail())
                    .password(webSecurityConfig.passwordEncoder().encode(attendeesDTO.getPassword()))
                    .isJoinOtherTour(false)
                    .isBuildOwnTour(false)
                    .isOrga(false)
                    .isPress(false)
                .build();

        try {
            attendees = attendeesRepository.save(attendees);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Could not save Attendees");
        }
        return attendees;
    }

    public Attendees updateAttendees(EditAttendeesDTO attendeesDTO) {
        Attendees attendees = attendeesRepository.findById((long) attendeesDTO.getId());
        attendees.setOrganisation(organisationRepository.findById((long) attendeesDTO.getOrganisation()));
        attendees.setCountry(countryRepository.findById((long) attendeesDTO.getCountry()));
        if (attendees.getOrganisation() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not find Organisation with given id");
        }
        if (attendees.getCountry() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not find Country with given id");
        }
        attendees.setFirstName(StringUtils.trimToEmpty(attendeesDTO.getFirstName()));
        attendees.setSecondName(StringUtils.trimToEmpty(attendeesDTO.getSecondName()));
        if (attendeesDTO.getMail() != attendees.getMail()) {
            if (attendeesRepository.countAllByMail(attendeesDTO.getMail()) > 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already used");
            }
            attendees.setMail(StringUtils.lowerCase(attendeesDTO.getMail()));
        }

        if (attendeesDTO.getUsername() != attendees.getUsername()) {
            if(attendeesRepository.countAllByUsername(attendeesDTO.getUsername())>0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Username already taken");
            }

            attendees.setUsername(StringUtils.trim(attendeesDTO.getUsername()));
        }

        try {
            attendees = attendeesRepository.save(attendees);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Could not save Attendees");
        }

        return attendees;
    }

    public Attendees updateAttendeesProfile(EditAttendeesProfileDTO attendeesDTO) {
        Attendees attendees = attendeesRepository.findById((long) attendeesDTO.getId());
        if (attendeesDTO.getMeanOfTransportation() != null) {
            attendees.setMeanOfTransportation(meanOfTransportationRepository.findById((long) attendeesDTO.getMeanOfTransportation()));
            if (attendees.getMeanOfTransportation() == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not find Country with given id");
            }
        }

        if (!StringUtils.isEmpty(attendeesDTO.getStartPoint().getName())) {
            TourPoint tourPoint = attendees.getStartPoint();
            if (tourPoint == null) {
                tourPoint = TourPoint.builder()
                    .latitude(attendeesDTO.getStartPoint().getLatitude())
                    .longitude(attendeesDTO.getStartPoint().getLongitude())
                    .date(attendeesDTO.getStartPoint().getDate())
                    .name(attendeesDTO.getStartPoint().getName())
                .build();
            } else {
                tourPoint.setLatitude(attendeesDTO.getStartPoint().getLatitude());
                tourPoint.setLongitude(attendeesDTO.getStartPoint().getLongitude());
                tourPoint.setDate(attendeesDTO.getStartPoint().getDate());
                tourPoint.setName(attendeesDTO.getStartPoint().getName());
            }
            attendees.setStartPoint(tourPoint);
        }

        attendees.setDescription(StringUtils.trimToEmpty(attendeesDTO.getDescription()));
        attendees.setDaysToParticipate(attendeesDTO.getDaysToParticipate());
        attendees.setIsJoinOtherTour(BooleanUtils.toBoolean(attendeesDTO.getIsJoinOtherTour()));
        attendees.setIsBuildOwnTour(BooleanUtils.toBoolean(attendeesDTO.getIsBuildOwnTour()));
        attendees.setIsPress(BooleanUtils.toBoolean(attendeesDTO.getIsPress()));
        attendees.setIsOrga(BooleanUtils.toBoolean(attendeesDTO.getIsOrga()));
        attendees.setDistanceToParticipate(attendeesDTO.getDistanceToParticipate());

        try {
            attendees = attendeesRepository.save(attendees);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Could not save Attendees");
        }

        return attendees;
    }

    public Attendees updateAttendeesPassword(EditAttendeesPasswordDTO attendeesDTO) {
        Attendees attendees = attendeesRepository.findById((long) attendeesDTO.getId());
        if (attendees.getPassword() != attendeesDTO.getFormerPassword()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Former Password miss match.");
        }
        if(StringUtils.isEmpty(attendeesDTO.getPassword()) || !attendeesDTO.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password dose not match conditions");
        }
        if (attendeesDTO.getPassword() != attendeesDTO.getPasswordConfirm()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password does not match the confirmation");
        }
        attendees.setPassword(webSecurityConfig.passwordEncoder().encode(attendeesDTO.getPassword()));

        try {
            attendees = attendeesRepository.save(attendees);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Could not save Attendees");
        }

        return attendees;
    }

    public Attendees getAttendees(Long id) {
        return attendeesRepository.findById(id).get();
    }
}
