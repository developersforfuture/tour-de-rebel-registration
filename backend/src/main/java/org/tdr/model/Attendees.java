package org.tdr.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(indexes = {
        @Index(name = "idx_id", columnList = "id", unique = true),
        @Index(name = "idx_username", columnList = "username", unique = true)
})
public class Attendees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, unique = true)
    private String mail;

    @Column(nullable = true)
    private String firstName;

    @Column(nullable = true)
    private String secondName;

    @ManyToOne
    private Organisation organisation;

    @ManyToOne
    private Country country;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Tour> tours;

    @Column(nullable = true)
    private String description;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
    private TourPoint startPoint;

    @Column(nullable = false)
    private int daysToParticipate = 0;

    @Column(nullable = false)
    private int distanceToParticipate = 0;

    @ManyToOne
    private MeanOfTransportation meanOfTransportation;

    @Column(nullable = false)
    private Boolean isJoinOtherTour = false;

    @Column(nullable = false)
    private Boolean isBuildOwnTour = false;

    @Column(nullable = false)
    private Boolean isPress = false;

    @Column(nullable = false)
    private Boolean isOrga = false;
}
