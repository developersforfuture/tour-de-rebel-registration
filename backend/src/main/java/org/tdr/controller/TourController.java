package org.tdr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tdr.Converter;
import org.tdr.dto.PageDTO;
import org.tdr.dto.TourDTO;
import org.tdr.model.Tour;
import org.tdr.service.TourService;
import org.tdr.utils.PageUtils;

@RestController
@RequestMapping("api/tour")
public class TourController {

    @Autowired
    private TourService tourService;

    @GetMapping("/find/{page}")
    public ResponseEntity<PageDTO<TourDTO>> getAll(@PathVariable Integer page){
        return findTours("",page);
    }

    @GetMapping("/find/{page}/{searchTerm}")
    public ResponseEntity<PageDTO<TourDTO>> findTours(@PathVariable String searchTerm, @PathVariable Integer page){
        if(page <= 0){
            return ResponseEntity.badRequest().build();
        }
        page-=1;

        Page<Tour> pageResult = tourService.findTeams(searchTerm,page);
        PageDTO<TourDTO> pageDTO = PageUtils.convertPage(pageResult,l->Converter.convertToursToTourDTOs(l,false));
        return ResponseEntity.ok(pageDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TourDTO> getTeam(@PathVariable long id){
        Tour tour = tourService.getTour(id);
        if(tour == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(Converter.convertTourToTourDTO(tour,true));
    }
}
