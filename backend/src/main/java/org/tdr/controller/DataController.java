package org.tdr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tdr.Converter;
import org.tdr.dto.CountryDTO;
import org.tdr.dto.MeanOfTransportationDTO;
import org.tdr.dto.OrganisationDTO;
import org.tdr.repository.CountryRepository;
import org.tdr.repository.MeanOfTransportationRepository;
import org.tdr.repository.TourRepository;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DataController {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private org.tdr.repository.OrganisationRepository organisationRepository;

    @Autowired
    private MeanOfTransportationRepository MeanOfTransportationRepository;

    @Autowired
    private TourRepository tourRepository;

    @GetMapping("/organisations")
    public List<OrganisationDTO> getAllOrganisations(){
        return Converter.convertOrganistationsToOrganistationDTOs(organisationRepository.findAll());
    }

    @GetMapping("/countries")
    public List<CountryDTO> getAllCountries(){
        return Converter.convertCountiesToCountyDTOs(countryRepository.findAll());
    }

    @GetMapping("/mean-of-transportations")
    public List<MeanOfTransportationDTO> getAllMeanOfTransportations(){
        return Converter.convertMeansOfTransportationToMeansOfTransportationDTOs(MeanOfTransportationRepository.findAll());
    }
}
