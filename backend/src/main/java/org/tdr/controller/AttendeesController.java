package org.tdr.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.tdr.Converter;
import org.tdr.dto.EditAttendeesDTO;
import org.tdr.dto.EditAttendeesPasswordDTO;
import org.tdr.dto.EditAttendeesProfileDTO;
import org.tdr.dto.RegistrationAttendeesDTO;
import org.tdr.dto.TeamDTO;
import org.tdr.model.Attendees;
import org.tdr.security.JwtResponse;
import org.tdr.security.JwtTokenUtil;
import org.tdr.security.JwtUserDetailsService;
import org.tdr.security.MyUserPrincipal;
import org.tdr.service.AttendeesService;
import org.tdr.service.TeamService;

import java.util.List;

@RestController
@RequestMapping("/api/attendees")
public class AttendeesController {

    @Autowired
    private AttendeesService attendeesService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;
    @Autowired
    private TeamService teamService;


    private void validateRegistrationAttendees(RegistrationAttendeesDTO registrationAttendeesDTO){
        if(StringUtils.isEmpty(registrationAttendeesDTO.getUsername()) || registrationAttendeesDTO.getUsername().length() < 4 || registrationAttendeesDTO.getUsername().length() > 20){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Username have to between 4 and 20 characters");
        }
        if(StringUtils.isEmpty(registrationAttendeesDTO.getPassword()) || !registrationAttendeesDTO.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password dose not match conditions");
        }
        if (!(new String(registrationAttendeesDTO.getPassword()).equals(registrationAttendeesDTO.getPasswordConfirm()))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password does not match the confirmation");
        }
        if(StringUtils.isEmpty(registrationAttendeesDTO.getMail()) || !registrationAttendeesDTO.getMail().matches("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Not a valid email format");
        }
    }

    private void validateEditAttendees(EditAttendeesDTO attendeesDTO) {
        if(StringUtils.isEmpty(attendeesDTO.getUsername()) || !StringUtils.isAlphanumeric(attendeesDTO.getUsername()) || attendeesDTO.getUsername().length() < 4 || attendeesDTO.getUsername().length() > 20){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Username have to between 4 and 20 characters and alphanumeric");
        }
        if(StringUtils.isEmpty(attendeesDTO.getMail()) || !attendeesDTO.getMail().matches("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Not a valid email format");
        }
    }

    private void validateEditAttendeesProfile(EditAttendeesProfileDTO registrationAttendeesDTO){
    }

    private void validateEditAttendeesPassword(EditAttendeesPasswordDTO attendeesDTO){
        if(StringUtils.isEmpty(attendeesDTO.getPassword()) || !attendeesDTO.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password dose not match conditions");
        }
        if (attendeesDTO.getPassword() != attendeesDTO.getPasswordConfirm()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password does not match the confirmation");
        }
    }

    @PostMapping
    ResponseEntity<EditAttendeesDTO> saveAttendees(@RequestBody EditAttendeesDTO attendeesDTO) {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (myUserPrincipal.getAttendees().getId() == attendeesDTO.getId()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        validateEditAttendees(attendeesDTO);
        Attendees attendees = attendeesService.updateAttendees(attendeesDTO);
        return ResponseEntity.ok(Converter.convertAttendeesToEditAttendeesDTO(attendees));
    }

    @PostMapping("/profile")
    ResponseEntity<EditAttendeesProfileDTO> saveEditAttendeesProfile(@RequestBody EditAttendeesProfileDTO attendeesDTO) {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (myUserPrincipal.getAttendees().getId() == attendeesDTO.getId()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        validateEditAttendeesProfile(attendeesDTO);
        Attendees attendees = attendeesService.updateAttendeesProfile(attendeesDTO);
        return ResponseEntity.ok(Converter.convertAttendeesToEditAttendeesProfileDTO(attendees));
    }

    @PostMapping("/password")
    ResponseEntity<EditAttendeesPasswordDTO> saveEditAttendeesPassword(@RequestBody EditAttendeesPasswordDTO attendeesDTO) {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (myUserPrincipal.getAttendees().getId() == attendeesDTO.getId()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        validateEditAttendeesPassword(attendeesDTO);
        Attendees attendees = attendeesService.updateAttendeesPassword(attendeesDTO);
        return ResponseEntity.ok(Converter.convertAttendeesToEditAttendeesPasswordDTO(attendees));
    }

    @PostMapping("/create")
    ResponseEntity<JwtResponse> createAttendees(@RequestBody RegistrationAttendeesDTO attendeesDTO) {
        validateRegistrationAttendees(attendeesDTO);
        Attendees attendees = attendeesService.createAttendees(attendeesDTO);
        if (attendees == null) {
            return ResponseEntity.badRequest().build();
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(attendeesDTO.getUsername(), attendeesDTO.getPassword()));
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(attendeesDTO.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping
    ResponseEntity<EditAttendeesDTO> getOwnEditAttendeesInfo() {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        EditAttendeesDTO attendeesDTO = Converter.convertAttendeesToEditAttendeesDTO(myUserPrincipal.getAttendees());
        return ResponseEntity.ok(attendeesDTO);
    }

    @GetMapping("/profile")
    ResponseEntity<EditAttendeesProfileDTO> getOwnEditAttendeesProfileInfo() {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        EditAttendeesProfileDTO attendeesDTO = Converter.convertAttendeesToEditAttendeesProfileDTO(myUserPrincipal.getAttendees());

        return ResponseEntity.ok(attendeesDTO);
    }

    @GetMapping("/teams")
    ResponseEntity<List<TeamDTO>> getTeams() {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return ResponseEntity
                .ok(Converter.convertTeamsToTeamDTOs(teamService.getTeamsByLeader(myUserPrincipal.getAttendees())));
    }
}
