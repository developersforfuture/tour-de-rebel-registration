package org.tdr.integration.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.tdr.repository.AttendeesRepository;
import org.tdr.dto.RegistrationAttendeesDTO;
import org.tdr.security.JwtRequest;
import org.tdr.security.JwtResponse;
import org.json.*;
import org.tdr.MainApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes=MainApplication.class)
@AutoConfigureMockMvc 
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureTestDatabase
//@Sql({"/test-data.sql"})
public class AttendeesControllerTest {

    @LocalServerPort
    private int port;
    
    @Autowired
    private MockMvc mvc;

    @Autowired
    private AttendeesRepository repository;

    private String jwtToken;

    TestRestTemplate restTemplate = new TestRestTemplate();
    
    HttpHeaders headers = new HttpHeaders();
    
    @After
    public void resetDb() {
        repository.deleteAll();
    }

   @Before
   public void setup() {
        JwtRequest dto = new JwtRequest();
        dto.setUsername("test");
        dto.setPassword("password");
        HttpEntity<JwtRequest> entity = new HttpEntity<JwtRequest>(dto, headers);

        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/login"), HttpMethod.POST, entity, String.class);

        try {
            JSONObject json = new JSONObject(response.getBody());
            jwtToken = json.getString("token");
        } catch (JSONException ex) {
            System.out.println(ex);
        }
   }
    @Test
    public void testCreateAttendeesReturnsToken() throws Exception {
        RegistrationAttendeesDTO dto = RegistrationAttendeesDTO.builder()
                .username("testmax")
                .password("Abcd&fg123")
                .passwordConfirm("Abcd&fg123")
                .mail("test@mail.de")
            .build();
        HttpEntity<RegistrationAttendeesDTO> entity = new HttpEntity<RegistrationAttendeesDTO>(dto, headers);

        ResponseEntity<String> response = restTemplate.exchange(
          createURLWithPort("/api/attendees/create"), HttpMethod.POST, entity, String.class);
          assertTrue(response.getBody().contains("token"));
    }
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
 
}